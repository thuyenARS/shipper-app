@extends('layouts.master')
@section('title', trans('Admin'))
@section('header')
@include('layouts.header', ['link' => URL::route('admin.account.getEdit', Auth::user()->id),
                            'username' => Auth::user()->name ])
@endsection
@section('sidebar')
@include('admin.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Information
        <small>order</small>
      </h1>
    </section>

    <div class="row">
        <div class="col-md-12">
            <hr>
            @if (Session::has('flash_message'))
                <div class="alert alert-{!! Session::get('flash_level') !!}">
                    {!!  Session::get('flash_message')!!}
                </div>
            @endif
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->

        <!-- table -->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Table With Full Features</h3>
            </div>
          <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped display"cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Commodity</th>
                            <th>Status</th>
                            <th>Type</th>
                            <th>Shipper</th>
                            <th>Customer</th>
                            <th>Recipient</th>
                            <th>Ware House</th>
                            <th>Cost</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $stt=0 ?>
                        @foreach($orders as $order)
                        <tr>
                            <td>{!! $order['id'] !!}</td>
                            <td>{!! $order['commodities']['name'] !!}</td>
                            <td>{!! $order['status'] !!}</td>
                            <td>{!! $order['type'] !!}</td>
                            <td>{!! $order['shipper']['users']['name'] !!}</td>
                            <td>{!! $order['customer']['users']['name'] !!}</td>
                            <td>{!! $order['recipient']['name'] !!}</td>
                            <td>{!! $order['ware_house']['name'] !!}</td>
                            <td>{!! $order['ship_cost']  !!}</td>
                            <td>                
                              @if( $order['status'] ==1)
                                  <a class="btn btn-danger btn-delete" href="{!! URL::route('admin.orders.delete', $order->id) !!}" onclick="return (window.confirm('Are you want to delete?') ? true : false)">Delete</a>
                              @elseif($order['status'] == 2)
                                  <a class="btn btn-success btn-edit" href="{!! URL::route('admin.orders.removeShipper', $order->id) !!}" onclick="return (window.confirm('Are you want to remove shipper?') ? true : false)">Renove</a>
                              @endif
                            </td>
                        </tr>
                        <?php $stt= $stt+1 ?>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Commodity</th>
                            <th>Status</th>
                            <th>Type</th>
                            <th>Shipper</th>
                            <th>Customer</th>
                            <th>Recipient</th>
                            <th>Ware House</th>
                            <th>Cost</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        <!-- /.box-body -->
        </div>
    <!-- end table -->
    </section>
<!-- /.content -->
</div>
@endsection