@extends('layouts.master')
@section('title', trans('Admin'))
@section('header')
@include('layouts.header', ['link' => URL::route('admin.account.getEdit', Auth::user()->id),
                            'username' => Auth::user()->name ])
@endsection
@section('sidebar')
@include('admin.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Create
      <small>order</small>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
<form>
  <!-- người nhận -->
  <fieldset class="form-group">
    <label><strong>1. Giao hàng tới người nhận</strong></label>
    <div> &nbsp;</div>
  <div class="col-md-6">
    <div class="form-group">
      <p>Phone</p>
      <input type="text" class="form-control" placeholder="Số điện thoại người nhận">
    </div>
    <div class="form-group">
      <p>Họ tên</p>
      <input type="text" class="form-control"  placeholder="Họ tên">
    </div>
  </div>
  <div class="col-md-6">
      <p>Địa chỉ</p>
      <div class="panel panel-default">
          <div class="panel-body text-center">
              
          </div>
      </div>
  </div>
  </fieldset>
  <!-- end người nhận -->

  <!-- kho hàng -->
  <fieldset class="form-group">
    <label><strong>2. Lấy hàng tại</strong></label>
    <div> &nbsp;</div>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
         <input id="radioStacked1" name="radio-stacked" type="radio" class="custom-control-input">
        <span class="custom-control-description">Chọn kho hàng</span>
        <div> &nbsp;</div>
        <select class="form-control">
          <option>option 1</option>
          <option>option 2</option>
          <option>option 3</option>
          <option>option 4</option>
          <option>option 5</option>
        </select>
      </div>
      <div class="form-group">
        <p>Phone</p>
        <input type="text" class="form-control" placeholder="Số điện thoại người nhận">
      </div>
      <div class="form-group">
        <p>Name</p>
        <input type="text" class="form-control"  placeholder="Họ tên">
      </div>
      <div class="form-group">
        <p>Địa chỉ</p>
        <input type="text" class="form-control"  placeholder="">
      </div>
      <div class="form-group">
        <p>Khu vực</p>
        <input type="text" class="form-control"  placeholder="">
      </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                
            </div>
        </div>
    </div>
  </div>
  <div class="row">
      <div class="col-md-6">
      <div class="form-group">
        <input id="radioStacked1" name="radio-stacked" type="radio" class="custom-control-input">
        <span class="custom-control-description">Nhập kho hàng</span>
        <div> &nbsp;</div>
        <select class="form-control">
          <option>option 1</option>
          <option>option 2</option>
          <option>option 3</option>
          <option>option 4</option>
          <option>option 5</option>
        </select>
      </div>
      <div class="form-group">
        <p>Phone</p>
        <input type="text" class="form-control" placeholder="Số điện thoại người nhận">
      </div>
      <div class="form-group">
        <p>Name</p>
        <input type="text" class="form-control"  placeholder="Họ tên">
      </div>
      <a class="btn btn-primary">Lưu kho hàng</a>
      <a class="btn btn-primary">Hủy</a>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                
            </div>
        </div>
    </div>
  </div>
  </fieldset>
  <!-- end kho hàng -->

  <!-- Hàng hóa -->
  <fieldset class="form-group">
    <label><strong>3. Hàng hóa</strong></label>
    <div> &nbsp;</div>
  <div class="col-md-6">
    <div class="form-group">
      <p>Name</p>
      <input type="text" class="form-control"  placeholder="Tên hàng">
    </div>
    <div class="form-group">
      <p>Count</p>
      <input type="text" class="form-control" placeholder="Số lượng">
    </div>
    <div class="form-group">
      <p>Price</p>
      <input type="text" class="form-control" placeholder="Trị giá">
    </div>
    <div class="form-group">
      <p>Weight</p>
      <input type="text" class="form-control" placeholder="Trọng lượng">
    </div>
  </div>
  </fieldset>
  <!-- end Hàng hóa -->

  <!-- lựa chọn dịch vụ -->
   <fieldset class="form-group">
    <label><strong>4. Dịch vụ</strong></label>
    <div> &nbsp;</div>
    <div class="col-md-6">
      <select class="form-control">
        <option>option 1</option>
        <option>option 2</option>
        <option>option 3</option>
        <option>option 4</option>
        <option>option 5</option>
      </select>
    </div>
  </fieldset>
  <fieldset class="form-group">
    <label><strong>5. Tính phí</strong></label>
    <div> &nbsp;</div>
    <a class="btn btn-primary">Tính phí</a>
    <div> &nbsp;</div>
    <p>Cost</p>
  </fieldset>
  <!-- end lựa chọn dịch vụ -->
  <button type="submit" class="btn btn-primary">Duyệt đơn hàng</button>
</form>



</section>
<!-- /.content -->
</div>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
@endsection