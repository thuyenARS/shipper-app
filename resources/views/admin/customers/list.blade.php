@extends('layouts.master')
@section('title', trans('Admin'))
@section('header')
@include('layouts.header', ['link' => URL::route('admin.account.getEdit', Auth::user()->id),
                            'username' => Auth::user()->name ])
@endsection
@section('sidebar')
@include('admin.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Information
      <small>Customer</small>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->

    <!-- table -->
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped display"cellspacing="0">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Address</th>
              <th>Balance</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @for($j=0; $j < count($customers); $j++)
            <tr>
              <td>{{ $customers[$j]['id'] }}</td>
              <td>{{ $customers[$j]['name'] }}</td>
              <td>{{ $customers[$j]['phone'] }}</td>
              <td>{{ $customers[$j]['email'] }}</td>
              <td>{{ $customers[$j]['address'] }}</td>
              <td>{!! number_format($customers[$j]['balances'], 0, ",", ".")  !!} VNĐ</td>
              <td>
                <a class="btn btn-success btn-edit" href="{!! URL::route('admin.customers.getEdit', $customers[$j]['id']) !!}"> Edit</a>
                <a class="btn btn-danger btn-delete" href="{!! URL::route('admin.customers.delete', $customers[$j]['id']) !!}" onclick="return (window.confirm('Are you want to delete?') ? true : false)"> Delete</a>
              </td>
            </tr>
            @endfor
        </tbody>
        <tfoot>
          <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Address</th>
              <th>Balance</th>
              <th>Action</th>
            </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>

  <!-- end table -->

  <!-- /.row -->

</section>
<!-- /.content -->
</div>
@endsection