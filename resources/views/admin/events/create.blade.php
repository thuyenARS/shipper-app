@extends('layouts.master')
@section('title', trans('Admin-Event'))
@section('header')
@include('layouts.header', ['link' => URL::route('admin.account.getEdit', Auth::user()->id),
                            'username' => Auth::user()->name ])
@endsection
@section('sidebar')
@include('admin.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Create
      <small>Events</small>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
  <form action="" method="POST">
   <input  type="hidden" name="_token" value="{!! csrf_token()!!}">
    <!-- create users -->
    <fieldset class="form-group">
      <div> &nbsp;</div>
    <div class="col-md-12">
      <div class="form-group">
        <p>Name</p>
        <input type="text" name="name" class="form-control"  placeholder="Event Name">
      </div>
      <div class="form-group">
        <p>Title</p>
        <input type="text" name="title" class="form-control" placeholder="Title">
      </div>
      <!-- textarea -->
      <div class="form-group">
        <label>Summary</label>
        <textarea class="form-control" name="summary" rows="3" placeholder="Summary"></textarea>
      </div>
    <button type="submit" class="btn btn-primary">Create</button>
    <button class="btn btn-primary btn-danger">Cancel</button>
    </div>
    </fieldset>
    <!-- end create users -->

  </form>
</section>
<!-- /.content -->
</div>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
@endsection