@extends('layouts.master')
@section('title', trans('Admin'))
@section('header')
@include('layouts.header', ['link' => URL::route('admin.account.getEdit', Auth::user()->id),
                            'username' => Auth::user()->name ])
@endsection
@section('sidebar')
@include('admin.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Information
      <small>order</small>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->

    <!-- table -->
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped display"cellspacing="0">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Title</th>
              <th>Summary</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($events as $event)
            <tr>
              <td>{{ $event->id }}</td>
              <td>{{ $event->name }}</td>
              <td>{{ $event->title }}</td>
              <td>{{ $event->summary }}</td>
              <td>
                <a class="btn btn-success btn-edit" href="{!! URL::route('admin.events.getEdit', $event['id']) !!}"> Edit</a>
                <a class="btn btn-danger btn-delete" href="{!! URL::route('admin.events.delete', $event['id']) !!}" onclick="return (window.confirm('Are you want to delete?') ? true : false)"> Delete</a>
              </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Title</th>
            <th>Summary</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>

  <!-- end table -->

  <!-- /.row -->

</section>
<!-- /.content -->
</div>
@endsection