@extends('layouts.master')
@section('title', trans('Admin'))
@section('header')
@include('layouts.header', ['link' => URL::route('customer.account.getEdit', Auth::user()->id),
                            'username' => Auth::user()->name ])
@endsection
@section('sidebar')
@include('admin.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Information
      <small>order</small>
    </h1>
  </section>

  <div class="row">
      <div class="col-md-12">
          <hr>
          @if (Session::has('flash_message'))
              <div class="alert alert-{!! Session::get('flash_level') !!}">
                  {!!  Session::get('flash_message')!!}
              </div>
          @endif
      </div>
  </div>
  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->

    <!-- table -->
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped display"cellspacing="0">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Address</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          @foreach($users as $item)
            <tr>
              <td>{{ $item->id }}</td>
              <td>{{ $item->name }}</td>
              <td>{{ $item->phone }}</td>
              <td>{{ $item->email }}</td>
              <td>{{ $item->address }}</td>
              <td>
                <a class="btn btn-success btn-edit" href="{!! URL::route('admin.admin.getEdit', $item->id) !!}"> Edit</a>
                <a class="btn btn-danger btn-delete" href="{!! URL::route('admin.admin.delete', $item->id) !!}" onclick="return (window.confirm('Are you want to delete?') ? true : false)"> Delete</a>
              </td> 
            </tr>           
            @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>ID</th>
            <th>Status</th>
            <th>Type</th>
            <th>Shipper</th>
            <th>Recipient</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>

  <!-- end table -->
</section>
<!-- /.content -->
</div>
@endsection