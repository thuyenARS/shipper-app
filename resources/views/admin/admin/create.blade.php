@extends('layouts.master')
@section('title', trans('Admin-Create'))
@section('header')
@include('layouts.header', ['link' => URL::route('customer.account.getEdit', Auth::user()->id),
                            'username' => Auth::user()->name ])
@endsection
@section('sidebar')
@include('admin.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Create
      <small>Admin</small>
    </h1>
  </section>
  <div> &nbsp;</div>
  <div class="row"> 
      <div class="col-md-12">
          @include('elements.error') 
      </div>
  </div>
  <!-- Main content -->
  <section class="content">
<form action="" method="POST">
  <input  type="hidden" name="_token" value="{!! csrf_token()!!}">
  <!-- create users -->
  <fieldset class="form-group">
    <div> &nbsp;</div>
  <div class="col-md-12">
    <div class="form-group">
      <p>Họ tên</p>
      <input type="text" name="name" class="form-control"  placeholder="Họ tên" value="{!! old('name')!!}">
    </div>
    <div class="form-group">
      <p>Phone</p>
      <input type="text" name="phone" class="form-control" placeholder="Số điện thoại người nhận" value="{!! old('phone')!!}">
    </div>
    <div class="form-group">
      <p>Email</p>
      <input type="text" name="email" class="form-control" placeholder="Email" value="{!! old('email')!!}">
    </div>
    <div class="form-group">
      <p>Address</p>
      <input type="text" name="address" class="form-control" placeholder="Địa chỉ" value="{!! old('address')!!}">
    </div>
    <div class="form-group">
      <p>Password</p>
      <input type="password" name="password" class="form-control" placeholder="********">
    </div>
    <div class="form-group">
      <p>Password Confirm</p>
      <input type="password" name="password_confirm" class="form-control" placeholder="********">
    </div>
  <button type="submit" class="btn btn-primary">Create</button>
  <button class="btn btn-primary btn-danger">Cancel</button>
  </div>
  </fieldset>
  <!-- end create users -->

</form>



</section>
<!-- /.content -->
</div>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
@endsection