@extends('layouts.master')
@section('title', trans('Admin'))
@section('header')
@include('layouts.header', ['link' => URL::route('admin.account.getEdit', Auth::user()->id),
                            'username' => Auth::user()->name ])
@endsection
@section('sidebar')
@include('admin.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit
      <small>shippers</small>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
<form action="" method="POST">
   <input  type="hidden" name="_token" value="{!! csrf_token()!!}">
  <!-- create users -->
  <fieldset class="form-group">
    <div> &nbsp;</div>
  <div class="col-md-12">
    <div class="form-group">
        <p>Họ tên</p>
        <div class="col-md-12">
          <input type="text" name="name" class="form-control"  placeholder="Họ tên" value="{{  old('name', isset($shipper) ? $shipper['name'] : null) }}">
        </div>
      </div>
      <div> &nbsp;</div>
      <div class="form-group">
        <p>Email</p>
        <div class="col-md-12">
          <input type="text" name="email" class="form-control" placeholder="Email" value="{{  old('email', isset($shipper) ? $shipper['email'] : null) }}">
        </div>
      </div>
      <div> &nbsp;</div>
      <div class="form-group">
        <p>Address</p>
        <div class="col-md-12">
          <input type="text" name="address" class="form-control" placeholder="Địa chỉ" value="{{  old('address', isset($shipper) ? $shipper['address'] : null) }}">
        </div>
      </div>
       <div> &nbsp;</div>
      <div class="form-group">
        <p>Phone</p>
        <div class="col-md-12">
          <input type="text" name="phone" class="form-control" placeholder="Số điện thoại" value="{{  old('phone', isset($shipper) ? $shipper['phone'] : null) }}">
        </div>
      </div>
      <div> &nbsp;</div>
    <div class="form-group">
      <p>New Password</p>
      <div class="col-md-12">
      <input type="password" name="password" class="form-control" placeholder="********">
      </div>
    </div>
    <div class="form-group">
      <p>Password Confirm</p>
      <div class="col-md-12">
      <input type="password" name="password_confirmation" class="form-control" placeholder="********">
      </div>
    </div>
    <div> &nbsp;</div>
    <div class="col-md-12">
  <button type="submit" class="btn btn-primary">Create</button>
  <button class="btn btn-primary btn-danger">Cancel</button>
  </div>
   </div>
  </fieldset>
  <!-- end create users -->

</form>



</section>
<!-- /.content -->
</div>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
@endsection