@extends('layouts.master')
@section('title', trans('Admin'))
@section('header')
@include('layouts.header', ['link' => URL::route('admin.account.getEdit', Auth::user()->id),
                            'username' => Auth::user()->name ])
@endsection
@section('sidebar')
@include('admin.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Information
      <small>Customer</small>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->

    <!-- table -->
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped display"cellspacing="0">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Address</th>
              <th>Balance</th>
              <th>Level</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @for($j=0; $j < count($shippers); $j++)
            <tr>
              <td>{{ $shippers[$j]['id'] }}</td>
              <td>{{ $shippers[$j]['name'] }}</td>
              <td>{{ $shippers[$j]['phone'] }}</td>
              <td>{{ $shippers[$j]['email'] }}</td>
              <td>{{ $shippers[$j]['address'] }}</td>
              <td>{!! number_format($shippers[$j]['balances'], 0, ",", ".")  !!} VNĐ</td>
              <td class="jRate" id="{{ $j }}"></td>
              <td>
                <a class="btn btn-success btn-edit" href="{!! URL::route('admin.shippers.getEdit', $shippers[$j]['id']) !!}"> Edit</a>
                <a class="btn btn-danger btn-delete" href="{!! URL::route('admin.shippers.delete', $shippers[$j]['id']) !!}" onclick="return (window.confirm('Are you want to delete?') ? true : false)"> Delete</a>
              </td>
            </tr>
            @endfor
        </tbody>
        <tfoot>
          <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Address</th>
              <th>Balance</th>
              <th>Level</th>
              <th>Action</th>
            </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>

  <!-- end table -->

  <!-- /.row -->

</section>
<!-- /.content -->
</div>
@endsection
@section('script_history')
<script>
    var i = 0;
    <?php for($j=0; $j < count($shippers); $j++): ?>    
      $("#" + i).jRate({
        <?php if ($shippers[$j]['level']!= NULL): ?>
          rating:{{ $shippers[$j]['level'] }},     
        <?php endif ?>
        readOnly: true,     
        backgroundColor: 'cyan',
        width: 20,
        height: 20,
        startColor: 'yellow',
        endColor: 'red'
    });    
      i++;
    <?php endfor ?>
</script>
@endsection