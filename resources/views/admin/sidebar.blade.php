<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{Asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ $username }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="treeview {{ Request::route()->getName() ==  'admin.orders.list' ? 'active' : '' }}">
        <a href="#">
          <i class="fa fa-reorder"></i>
          <span>Manager Order</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Request::route()->getName() ==  'admin.orders.list' ? 'active' : '' }}"><a href="{!! URL::route('admin.orders.list') !!}"><i class="fa fa-circle-o"></i>List Order</a></li>
        </ul>
      </li>
      <li class="treeview {{ Request::route()->getName() ==  'admin.customers.list' ? 'active' : '' }}
                          {{ Request::route()->getName() ==  'admin.customers.getCreate' ? 'active' : '' }}
                          {{ Request::route()->getName() ==  'admin.customers.getEdit' ? 'active' : '' }}
                          ">
        <a href="#">
          <i class="fa  fa-female"></i>
          <span>Manager Customer</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Request::route()->getName() ==  'admin.customers.list' ? 'active' : '' }}"><a href="{!! URL::route('admin.customers.list') !!}"><i class="fa fa-circle-o"></i>List Customer</a></li>
          <li class="{{ Request::route()->getName() ==  'admin.customers.getCreate' ? 'active' : '' }}"><a href="{!! URL::route('admin.customers.getCreate') !!}"><i class="fa fa-circle-o"></i>Add Customer</a></li>
          <li class="{{ Request::route()->getName() ==  'admin.customers.getEdit' ? 'active' : 'disabled' }}"><a href=""><i class="fa fa-circle-o"></i>Edit Customer</a></li>
        </ul>
      </li>
      <li class="{{ Request::route()->getName() ==  'admin.shippers.list' ? 'active' : '' }}
                 {{ Request::route()->getName() ==  'admin.shippers.getCreate' ? 'active' : '' }}
                 {{ Request::route()->getName() ==  'admin.shippers.getEdit' ? 'active' : '' }}
      treeview">
        <a href="#">
          <i class="fa fa-ship"></i>
          <span>Manager Shipper</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Request::route()->getName() ==  'admin.shippers.list' ? 'active' : '' }}"><a href="{!! URL::route('admin.shippers.list') !!}"><i class="fa fa-circle-o"></i>List Shipper</a></li>
          <li class="{{ Request::route()->getName() ==  'admin.shippers.getCreate' ? 'active' : '' }}"><a href="{!! URL::route('admin.shippers.getCreate') !!}"><i class="fa fa-circle-o"></i>Add Shipper</a></li>
          <li class="{{ Request::route()->getName() ==  'admin.shippers.getEdit' ? 'active' : 'disabled' }}"><a href=""><i class="fa fa-circle-o"></i>Edit Shipper</a></li>
        </ul>
      </li>

      <li class="{{ Request::route()->getName() ==  'admin.events.list' ? 'active' : '' }}
                 {{ Request::route()->getName() ==  'admin.events.getCreate' ? 'active' : '' }}
                 {{ Request::route()->getName() ==  'admin.events.getEdit' ? 'active' : '' }}
                 treeview">
        <a href="#">
          <i class="fa fa-laptop"></i>
          <span>Manager Events</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Request::route()->getName() ==  'admin.events.list' ? 'active' : '' }}"><a href="{!! URL::route('admin.events.list') !!}"><i class="fa fa-circle-o"></i>List Events</a></li>
          <li class="{{ Request::route()->getName() ==  'admin.events.getCreate' ? 'active' : '' }}"><a href="{!! URL::route('admin.events.getCreate') !!}"><i class="fa fa-circle-o"></i>Add Events</a></li>
          <li class="{{ Request::route()->getName() ==  'admin.events.getEdit' ? 'active' : 'disabled' }}"><a href=""><i class="fa fa-circle-o"></i>Edit Events</a></li>
        </ul>
      </li>

      <li class="{{ Request::route()->getName() ==  'admin.admin.list' ? 'active' : '' }}
                 {{ Request::route()->getName() ==  'admin.admin.getCreate' ? 'active' : '' }}
                 {{ Request::route()->getName() ==  'admin.admin.getEdit' ? 'active' : '' }}
      treeview">
        <a href="#">
          <i class="fa fa-users"></i>
          <span>Admin Account</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Request::route()->getName() ==  'admin.admin.list' ? 'active' : '' }}"><a href="{!! URL::route('admin.admin.list') !!}"><i class="fa fa-circle-o"></i>List </a></li>
          <li class="{{ Request::route()->getName() ==  'admin.admin.getCreate' ? 'active' : '' }}"><a href="{!! URL::route('admin.admin.getCreate') !!}"><i class="fa fa-circle-o"></i>Create</a></li>
          <li class="{{ Request::route()->getName() ==  'admin.admin.getEdit' ? 'active' : 'disabled' }}"><a href="#"><i class="fa fa-circle-o"></i>Edit</a></li>
        </ul>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
<style type="text/css">
    li.disabled {
        pointer-events: none;
        cursor: default;
    }
</style>