<!DOCTYPE html>
<html>
  <head>
    <title>Talking with Pusher</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <div class="content">
        <h1>Laravel 5 and Pusher is fun!</h1>
        <ul id="messages" class="list-group">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        </ul>
      </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://js.pusher.com/3.1/pusher.min.js"></script>
    <script>
      //instantiate a Pusher object with our Credential's key
      // var pusher = new Pusher(
      //   '72f8c2eed83c7dd0f357',
      //   {
      //     encrypted: true
      //   }
      // );
      // var channel = pusher.subscribe('private-test-channel');
      // channel.bind('test-event', function(data) {
      //   alert(data.text);
      // });
      // using jQuery
      Pusher.log = function(msg){
        if (window.console && window.console.log) {
          window.console.log(msg);
        }
      }
      var pusher = new Pusher('72f8c2eed83c7dd0f357', {
            encrypted: true,
            authEndpoint: '/pusher/auth', // just a helper method to create a link
            auth: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
        });

        var channelName =  'private-notifications-'+16; // channel for the user id
        var channel = pusher.subscribe(channelName);

        channel.bind('new_notification', function (data)
        {
            alert("aaaaaaaaaaa");
            });
    </script>
  </body>
</html>