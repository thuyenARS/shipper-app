<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login BShip</title>  
      <link rel="stylesheet" href="{{Asset('plugins/login/css/style.css')}}">  
</head>

<body style="background-image: url(plugins/login/login.jpg);
  background-size: cover;">
<div class="container">
  <section id="content">
  <div class="row"> 
            <div class="col-md-12">
                @include('elements.error') 
            </div>
        </div>
        <div class="row">               
            @if(Session::has('flash_message'))
                <div class="alert alert-{{ Session::get('flash_level') }} col-md-12">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
        </div>
     <form action="" method="POST">
        <input  type="hidden" name="_token" value="{!! csrf_token()!!}">
      <h1>Login Form</h1>
      <div>
        <input type="text" name="email" placeholder="Username" required="" id="username" />
      </div>
      <div>
        <input type="password" name="password" placeholder="Password" required="" id="password" />
      </div>
      <div class="col-md-6">
        <input type="submit" value="Log in" />
      </div>
      <div class="col-md-6">        
        <a href="{!! URL('register') !!}">Register a new customer</a>
      </div>
    </form><!-- form -->
    <div class="button">
      <h1>BShip</h1>
    </div><!-- button -->
  </section><!-- content -->
</div><!-- container -->
</body>
  
    <script src="{{Asset('plugins/login/js/index.js')}}"></script>

</body>
</html>