<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Register Bship</title>  
      <link rel="stylesheet" href="{{Asset('plugins/login/css/style.css')}}">  
</head>

<body style="background-image: url(plugins/login/login.jpg);
  background-size: cover;">
<div class="container">
  <section id="content">
  <div class="row"> 
            <div class="col-md-12">
                @include('elements.error') 
            </div>
        </div>
        <div class="row">               
            @if(Session::has('flash_message'))
                <div class="alert alert-{{ Session::get('flash_level') }} col-md-12">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
        </div>
     <form action="" method="POST">
        <input  type="hidden" name="_token" value="{!! csrf_token()!!}">
      <h1>Login Form</h1>
      <div class="form-group has-feedback">
        <input type="text" name="name" class="form-control" placeholder="Full name" >
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="email" class="form-control" placeholder="Email" >
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="phone" class="form-control" placeholder="Phone" >
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="address" class="form-control" placeholder="Address" >
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="acount_id" class="form-control" placeholder="Account" >
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password_confirm" class="form-control" placeholder="Retype password" >
      </div>
      <div class="col-md-6">
        <input type="submit" value="Register" />
      </div >
      <div>
        <a href="{!! URL('login') !!}">Login Bship</a>
      </div>
    </form><!-- form -->
    <div class="button">
      <h1>BShip</h1>

      <span class="fa-truck"></span>

    </div><!-- button -->
  </section><!-- content -->
</div><!-- container -->
</body>
  
    <script src="{{Asset('plugins/login/js/index.js')}}"></script>

</body>
</html>
