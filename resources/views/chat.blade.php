

<!-- <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script> -->
<script src="https://code.jquery.com/jquery-2.2.4.js"></script>
<link href="{{Asset('css/bootstrap.min.css')}}" rel="stylesheet">

 <script>
        $(document).on('keydown', '.send', function(e) {
        var msg = $('.send').val();
        var element = $(this);
        if (!msg ==  ''  && e.keyCode == 13 && !e.shiftKey) {
            // $('.chat-box').append('<br><div style="text-align: right">'+ msg+'</div><br>');
            $.ajax({
                url: '{{ url('sendMessage')}}',
                type: 'POST',
                cache: false,
                data: {_token: '{{ csrf_token() }}', msg: msg}
                // success:function(data){
                //     alert(data);
                // },error:function(){ 
                //     alert("error!!!!");
                // }
            });
            element.val('');
        }
    });
        $(function(){
        liveChat();
    });
    function liveChat() {
     $.ajax({
         url: '{{ url('ajax')}}',
         type: 'GET',
         cache:false,
         data: {_token: "{{ csrf_token() }}" },
         // dataType: 'text',
         success:function(data)
         {
            // alert("Hello! I am an alert box!!");
            // $('.chat-box').append('<br><div style="text-align: right">'+ data['msg']+'</div><br>');
             // $('.chat-box').append('<div class="alert alert-info">'+ "assssss" +'</div>');

             if(data != "axa"){
                    alert("Hello! I am an alert box!!");
                    $('.chat-box').append('<div class="alert alert-info">'+ data +'</div>');
                }else{
                    alert("Error ! Please contact Admin");
                    // $('.chat-box').append('<div class="alert alert-info">'+ data +'</div>');
                }
             setTimeout(liveChat, 1000);
         },
         error:function()
         {
             setTimeout(liveChat, 5000);
         }
     });
    }
    </script>
    <div class="container">
    		 	<h1 id="greeting">Hello, <span id="username">{{$username}}</span></h1>
    	<div class="row">
    	<div class="col-md-6">
    		<div class="chat-box">
    		 	@foreach($tests as $test)
    				<div class="alert alert-info">{{ $test->msg }}</div>
    			@endforeach
    		</div>
                <input  type="hidden" name="_token" value="{!! csrf_token()!!}">
                <input type="text" class="form-control send" name="">
    	</div>
    </div>
   