@extends('layouts.master')
@section('title', trans('Customer'))
@section('header')
@include('layouts.header', ['link' => URL::route('customer.account.getEdit', Auth::user()->id), 
                            'receives' => $receives,
                            'deliveries' => $deliveries,
                            'username' => Auth::user()->name
                             ])
@endsection
@section('sidebar')
@include('customer.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Information
      <small>order</small>
    </h1>
  </section>
  <div class="row">
      <div class="col-md-12">
          <hr>
          @if (Session::has('flash_message'))
              <div class="alert alert-{!! Session::get('flash_level') !!}">
                  {!!  Session::get('flash_message')!!}
              </div>
          @endif
      </div>
  </div>
  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Order Success</span>
            <span class="info-box-number">{{ round(100 * $success->count() / $orders->count()) }}<small>%</small></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Order Receive</span>
            <span class="info-box-number">{{ round(100 * $receives->count() / $orders->count()) }}<small>%</small></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->

      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Order Delivery</span>
            <span class="info-box-number">{{ round(100 * $deliveries->count() / $orders->count()) }}<small>%</small></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Order Waiting</span>
            <span class="info-box-number">{{ round( 100 * $wating->count() / $orders->count()) }}<small>%</small></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>

    <!-- table -->
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped display"cellspacing="0">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Type</th>
              <th>Shipper</th>
              <th>Cost</th>
              <th>Rating</th>
            </tr>
          </thead>
          <tbody>
          <?php $stt=0 ?>
          @foreach($success as $order)
            <tr>
              <td>{!! $order['id'] !!}</td>
              <td>{!! $order['name'] !!}</td>
              <td>{!! $order['type'] !!}</td>
              <td>{!! $nameShipper[$stt] !!}</td>
              <td>{!! number_format($order['ship_cost'], 0, ",", ".")  !!} VNĐ</td>
              <td>
                  <input  type="hidden"  name="_token" value="{!! csrf_token()!!}">
                  <div class="jRate" id="{{ $stt }}" ></div>
              </td>
            </tr>
            <?php $stt= $stt+1 ?>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Type</th>
            <th>Shipper</th>
            <th>Cost</th>
            <th>Rating</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>

  <!-- end table -->

  <!-- /.row -->

</section>
<!-- /.content -->
</div>
@endsection
@section('script_history')
<script>
    var i = 0;
    <?php foreach ($success as $order): ?>    
      $("#" + i).jRate({
        <?php if ($order['rate'] != NULL): ?>
          rating:{{ $order['rate'] }},     
          readOnly: true,     
        <?php endif ?>
        backgroundColor: 'cyan',
        width: 40,
        height: 40,
        startColor: 'yellow',
        endColor: 'red',
        onSet: function(rating) {
             $.ajax({
                    url : '{{ URL::route("customer.orders.rating") }}',
                    type : "post",
                    dateType:"text",
                    data : {
                          _token: '{{ csrf_token() }}',
                         rate : rating, 
                         order_id : {{ $order['id'] }}
                    }
                });
        }
    });    
      i++;
    <?php endforeach ?>
</script>
@endsection