@extends('layouts.master')
@section('title', trans('Customer'))
@section('header')
@include('layouts.header', ['link' => URL::route('customer.account.getEdit', Auth::user()->id), 
                            'receives' => $receives,
                            'deliveries' => $deliveries,
                            'username' => Auth::user()->name
                             ])
@endsection
@section('sidebar')
@include('customer.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Information
      <small>order</small>
    </h1>
  </section>
  <div class="row">
      <div class="col-md-12">
          <hr>
          @if (Session::has('flash_message'))
              <div class="alert alert-{!! Session::get('flash_level') !!}">
                  {!!  Session::get('flash_message')!!}
              </div>
          @endif
      </div>
  </div>
  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="icon ion-ios-checkmark"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Order Success</span>
            <span class="info-box-number">{{ round(100 * $success->count() / $orders->count()) }}<small>%</small></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-red"><i class="icon ion-social-twitter-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Order Receive</span>
            <span class="info-box-number">{{ round(100 * $receives->count() / $orders->count()) }}<small>%</small></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->

      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="icon ion-social-snapchat-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Order Delivery</span>
            <span class="info-box-number">{{ round(100 * $deliveries->count() / $orders->count()) }}<small>%</small></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="icon ion-ios-ionic-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Order Wating</span>
            <span class="info-box-number">{{ round( 100 * $wating->count() / $orders->count()) }}<small>%</small></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>

    <!-- table -->
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped display"cellspacing="0">
          <thead>
            <tr>
              <th>ID</th>
              <th>Commodity</th>
              <th>Status</th>
              <th>Type</th>
              <th>Shipper</th>
              <th>Recipient</th>
              <th>Ware House</th>
              <th>Cost</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          <?php $stt=0 ?>
          @foreach($orders as $order)
          @if($order['status'] != 6)
            <tr>
              <td>{!! $order['id'] !!}</td>
              <td>{!! $order['commodities']['name'] !!}</td>
              <td>{!! $order['status'] !!}</td>
              <td>{!! $order['type'] !!}</td>
              <td>{!! $nameShipper[$stt] !!}</td>
              <td>{!! $order['recipient']['name'] !!}</td>
              <td>{!! $order['ware_house']['name'] !!}</td>
              <td>{!! number_format($order['ship_cost'], 0, ",", ".")  !!} VNĐ</td>
              <td>
                @if( $order['status'] ==1)
                    <a class="btn btn-success btn-edit" href="{!! URL::route('customer.orders.getEdit', $order->id) !!}"> Edit</a>
                    <a class="btn btn-danger btn-delete" href="{!! URL::route('customer.orders.delete', $order->id) !!}" onclick="return (window.confirm('Are you want to delete?') ? true : false)"> Delete</a>
                @elseif( $order['status'] == 3)
                    <a class="btn btn-primary" href="{!! URL::route('customer.orders.confirm', $order->id) !!}" onclick="return (window.confirm('Are you want to confirm?') ? true : false)">Confirm</a>
                @elseif( $order['status'] == 5)
                    <a class="btn btn-primary" href="{!! URL::route('customer.orders.confirm', $order->id) !!}" onclick="return (window.confirm('Are you want to confirm?') ? true : false)">Confirm</a>
                @endif
              </td>
            </tr>
            @endif
            <?php $stt= $stt+1 ?>
            @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>ID</th>
            <th>Commodity</th>
            <th>Status</th>
            <th>Type</th>
            <th>Shipper</th>
            <th>Recipient</th>
            <th>Ware House</th>
            <th>Cost</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>

  <!-- end table -->

  <!-- /.row -->

</section>
<!-- /.content -->
</div>
@section('script_notification')
<script>
</script>
@endsection
@endsection