@extends('layouts.master')
@section('title', trans('Customer'))
@section('header')
@include('layouts.header', ['link' => URL::route('customer.account.getEdit', Auth::user()->id) ])
@endsection
@section('sidebar')
@include('customer.sidebar')
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Information
      <small>order</small>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Order Success</span>
            <span class="info-box-number">90<small>%</small></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Likes</span>
            <span class="info-box-number">41,410</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->

      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Sales</span>
            <span class="info-box-number">760</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">New Members</span>
            <span class="info-box-number">2,000</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>

    <!-- table -->
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped display"cellspacing="0">
          <thead>
            <tr>
              <th>ID</th>
              <th>Status</th>
              <th>Type</th>
              <th>Shipper</th>
              <th>Recipient</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>234
              </td>
              <td>Đã nhận hàng</td>
              <td>2</td>
              <td>Shipper1</td>
              <td>Recipient1</td>
              <td>
                <label for="input-1" class="control-label">Rate This</label>
                <input id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="5" data-step="1">
              </td>
            </tr>
            <tr>
              <td>234
              </td>
              <td>Giao hàng thành công</td>
              <td>2</td>
              <td>Shipper1</td>
              <td>Recipient1</td>
              <td>
                <label for="input-1" class="control-label">Rate This</label>
                <input id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="5" data-step="1">
              </td>
            </tr>
            <tr>
              <td>234
              </td>
              <td>Đã nhận hàng</td>
              <td>2</td>
              <td>Shipper1</td>
              <td>Recipient1</td>
              <td>
                <label for="input-1" class="control-label">Rate This</label>
                <input id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="5" data-step="1">
              </td>
            </tr><tr>
            <td>234
            </td>
            <td>Đã nhận hàng</td>
            <td>2</td>
            <td>Shipper3</td>
            <td>Recipient3</td>
            <td>
             <label for="input-1" class="control-label">Rate This</label>
             <input id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="5" data-step="1">
            </td>
          </tr>

        </tbody>
        <tfoot>
          <tr>
            <th>ID</th>
            <th>Status</th>
            <th>Type</th>
            <th>Shipper</th>
            <th>Recipient</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>

  <!-- end table -->

  <!-- /.row -->

</section>
<!-- /.content -->
</div>
@endsection