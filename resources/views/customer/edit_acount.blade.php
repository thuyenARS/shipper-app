@extends('layouts.master')
@section('title', trans('Customer'))
@section('header')
@include('layouts.header', ['link' => URL::route('customer.account.getEdit', Auth::user()->id),
                            'username' => Auth::user()->name ])
@endsection
@section('sidebar')
@include('customer.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Update
      <small>Account</small>
    </h1>
  </section>
  <div> &nbsp;</div>
  <div class="row"> 
      <div class="col-md-12">
          @include('elements.error') 
      </div>
  </div>
  <!-- Main content -->
  <section class="content">
 <form action="" method="POST">
    <input  type="hidden" name="_token" value="{!! csrf_token()!!}">
  <!-- người nhận -->
  <fieldset class="form-group">
    <div> &nbsp;</div>
  <div class="col-md-12">
    <div class="form-group">
        <p>Họ tên</p>
        <div class="col-md-12">
          <input type="text" name="name" class="form-control"  placeholder="Họ tên" value="{{  old('name', isset($customer) ? $customer['name'] : null) }}">
        </div>
      </div>
      <div> &nbsp;</div>
      <div class="form-group">
        <p>Email</p>
        <div class="col-md-12">
          <input type="text" name="email" class="form-control" placeholder="Email" value="{{  old('email', isset($customer) ? $customer['email'] : null) }}">
        </div>
      </div>
      <div> &nbsp;</div>
      <div class="form-group">
        <p>Address</p>
        <div class="col-md-12">
          <input type="text" name="address" class="form-control" placeholder="Địa chỉ" value="{{  old('address', isset($customer) ? $customer['address'] : null) }}">
        </div>
      </div>
       <div> &nbsp;</div>
      <div class="form-group">
        <p>Phone</p>
        <div class="col-md-12">
          <input type="text" name="phone" class="form-control" placeholder="Số điện thoại" value="{{  old('phone', isset($customer) ? $customer['phone'] : null) }}">
        </div>
      </div>
      <div> &nbsp;</div>
    <div class="form-group">
      <p>New Password</p>
      <div class="col-md-12">
      <input type="password" name="password" class="form-control" placeholder="********">
      </div>
    </div>
    <div class="form-group">
      <p>Password Confirm</p>
      <div class="col-md-12">
      <input type="password" name="password_confirmation" class="form-control" placeholder="********">
      </div>
    </div>
    <div> &nbsp;</div>
    <div class="col-md-12">
  </div>
   </div>
  </fieldset>
  <!-- end người nhận -->

  <button type="submit" class="btn btn-primary">Change</button>
</form>



</section>
<!-- /.content -->
</div>
@endsection