@extends('layouts.master')
@section('title', trans('Customer'))
@section('header')
@include('layouts.header', ['link' => URL::route('customer.account.getEdit', Auth::user()->id),
                            'username' => Auth::user()->name ])
@endsection
@section('sidebar')
@include('customer.sidebar', ['username' => Auth::user()->name])
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit
      <small>order</small>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
<form action="" method="POST">
  <input  type="hidden" name="_token" value="{!! csrf_token()!!}">
  
  <!-- Đơn hàng -->
  <fieldset class="form-group">
    <div class="col-md-6">
      <div class="form-group">
        <p>Đơn hàng</p>
        <input type="text" name="name" class="form-control" placeholder="Đơn hàng" value="{!!  old('name', isset($order) ? $order['name'] : null) !!}">
      </div>
    </div>
  </fieldset>

  <!-- người nhận -->
  <fieldset class="form-group">
    <label><strong>1. Giao hàng tới người nhận</strong></label>
    <div> &nbsp;</div>
  <div class="col-md-6">
    <div class="form-group">
      <p>Phone</p>
      <input type="text" name="phone_recip" class="form-control" placeholder="Số điện thoại người nhận" value="{!!  old('phone_recip', isset($recipient) ? $recipient['phone'] : null) !!}">
    </div>
    <div class="form-group">
      <p>Họ tên</p>
      <input type="text" name="name_recip" class="form-control"  placeholder="Họ tên" value="{!! old('name_recip', isset($recipient) ? $recipient['name'] : null) !!}">
    </div>
  </div>
  <div class="col-md-6">
      <p>Địa chỉ</p>
      <div class="panel panel-default">
          <input type="text" name="address_recip" id="address_recip" class="form-control"  placeholder="Địa chỉ" value="{!! old('address_recip', isset($recipient) ? $recipient['address'] : null) !!}">
          <input type="hidden" name="lat_recip" id="lat_recip" class="form-control"  placeholder="Lat" value="{!! old('lat_recip', isset($recipient) ? $recipient['latitude'] : null) !!}">
          <input type="hidden" name="long_recip" id="long_recip" class="form-control"  placeholder="Long" value="{!! old('long_recip', isset($recipient) ? $recipient['longtitude'] : null) !!}">
          <div class="panel-body text-center" id="map-recip" style="height: 300px;width: 100%;">
          </div>
      </div>
  </div>
  </fieldset>
  <!-- end người nhận -->

  <!-- kho hàng -->
  <fieldset class="form-group">
    <label><strong>2. Lấy hàng tại</strong></label>
    <div> &nbsp;</div>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
         <input id="radioStacked1" name="radio-stacked" type="radio" value="1" class="custom-control-input">
        <span class="custom-control-description">Chọn kho hàng</span>
        <div> &nbsp;</div>
        <select class="form-control select-ware-house" name="ware_house_id">
        @foreach( $ware_houses as $warehouse)
          <option value="{!! $warehouse->id !!}">{!! $warehouse->name !!}</option>
        @endforeach
        </select>
      </div>
    </div>
  </div>
  <div class="row">
      <div class="col-md-6">
      <div class="form-group">
        <input id="radioStacked1" name="radio-stacked" type="radio" value="2" class="custom-control-input">
        <span class="custom-control-description">Nhập kho hàng</span>
        <div> &nbsp;</div>
      </div>
      <div class="form-group">
        <p>Phone</p>
        <input type="text" class="form-control input-ware-house" name="phone_wh" placeholder="Số điện thoại người nhận">
      </div>
      <div class="form-group">
        <p>Name</p>
        <input type="text" class="form-control input-ware-house" name="name_wh" placeholder="Họ tên">
      </div>
    </div>
    <div class="col-md-6">
    <p>Địa chỉ</p>
        <div class="panel panel-default">
            <input type="text" name="address_wh" id="address_wh" class="form-control input-ware-house">
            <input type="hidden" name="lat_wh" id="lat_wh" class="form-control input-ware-house">
            <input type="hidden" name="long_wh" id="long_wh" class="form-control input-ware-house">
            <div class="panel-body text-center" id="map-wh" style="height: 300px;width: 100%;">
            </div>
        </div>
    </div>
  </div>
  </fieldset>
  <!-- end kho hàng -->

  <!-- Hàng hóa -->
  <fieldset class="form-group">
    <label><strong>3. Hàng hóa</strong></label>
    <div> &nbsp;</div>
  <div class="col-md-6">
    <div class="form-group">
      <p>Name</p>
      <input type="text" name="name-commodity" class="form-control"  placeholder="Tên hàng" value="{!! old('name-commodity', isset($commodity) ? $commodity['name'] : null) !!}">
    </div>
    <div class="form-group">
      <p>Count</p>
      <input type="text" name="count-commodity" class="form-control" placeholder="Số lượng" value="{!! old('count-commodity', isset($commodity) ? $commodity['count'] : null) !!}">
    </div>
    <div class="form-group">
      <p>Price</p>
      <input type="text" name="price-commodity" class="form-control" placeholder="Trị giá" value="{!! old('price-commodity', isset($commodity) ? $commodity['price'] : null) !!}">
    </div>
    <div class="form-group">
      <p>Weight</p>
      <input type="text" name="weight-commodity" class="form-control" placeholder="Trọng lượng" value="{!! old('weight-commodity', isset($commodity) ? $commodity['weigh'] : null) !!}">
    </div>
  </div>
  </fieldset>
  <!-- end Hàng hóa -->

  <!-- lựa chọn dịch vụ -->
   <fieldset class="form-group">
    <label><strong>4. Dịch vụ</strong></label>
    <div> &nbsp;</div>
    <div class="col-md-6">
      <select class="form-control" name="type">
        <option value="1">Chỉ giao hàng</option>
        <option value="2">Thu tiền hộ</option>
      </select>
    </div>
  </fieldset>
  <fieldset class="form-group">
    <label><strong>5. Tính phí</strong></label>
    <div> &nbsp;</div>
    <a class="btn btn-primary">Tính phí</a>
    <div> &nbsp;</div>
    <p>Cost</p>
  </fieldset>
  <!-- end lựa chọn dịch vụ -->
  <button type="submit" class="btn btn-primary">Duyệt đơn hàng</button>
</form>
</section>
<!-- /.content -->
</div>
@endsection
@section('script_edit_order')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzLBb9EoqY_rus9SSaUELEDw4RN8sm_tQ&libraries=places&callback=initAutocomplete">
    </script>
<script>
  $(document).ready(function() {
    $('.input-ware-house').attr('disabled', 'true');
    $("#radioStacked1").attr('checked', 'true');
        $('input[type=radio][name=radio-stacked]').change(function() {
            if (this.value == 2) {
                $('.select-ware-house').attr('disabled', 'true');
                $('.input-ware-house').removeAttr('disabled');
            } else {
                $('.input-ware-house').attr('disabled', 'true');
                $('.select-ware-house').removeAttr('disabled');
            }
        });
    });

  function initAutocomplete() {
        var uluru = {lat: 21.0054253, lng: 105.8475569};

        // Recipient
        var map = new google.maps.Map(document.getElementById('map-recip'), {
          zoom: 15,
          center: uluru,
          mapTypeId: 'roadmap'
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
        var searchBoxRecipient = new google.maps.places.SearchBox(document.getElementById('address_recip'));
        google.maps.event.addListener(searchBoxRecipient, 'places_changed', function() {

          var placesRecipient = searchBoxRecipient.getPlaces();

          var boundsRecipient = new google.maps.LatLngBounds();
          var i, place;

          for (var i = 0; place = placesRecipient[i]; i++) {
            // console.log(place.geometry.location.toJSON());
            $('#lat_recip').val(place.geometry.location.lat());
            $('#long_recip').val(place.geometry.location.lng());
            var lat_wh
            boundsRecipient.extend(place.geometry.location);
            marker.setPosition(place.geometry.location);
          }

          map.fitBounds(boundsRecipient);
          map.setZoom(15);
        });

        // warehouse
        var mapWareHouse = new google.maps.Map(document.getElementById('map-wh'), {
          zoom: 15,
          center: uluru,
          mapTypeId: 'roadmap'
        });

        var markerWareHouse = new google.maps.Marker({
          position: uluru,
          map: mapWareHouse
        });

        var searchBoxWareHouse = new google.maps.places.SearchBox(document.getElementById('address_wh'));
        google.maps.event.addListener(searchBoxWareHouse, 'places_changed', function() {

          var placesWareHouse = searchBoxWareHouse.getPlaces();

          var boundsWareHouse = new google.maps.LatLngBounds();
          var i, place;

          for (var i = 0; place = placesWareHouse[i]; i++) {
            // console.log(place.geometry.location.toJSON());
            $('#lat_wh').val(place.geometry.location.lat());
            $('#long_wh').val(place.geometry.location.lng());
            var lat_wh
            boundsWareHouse.extend(place.geometry.location);
            markerWareHouse.setPosition(place.geometry.location);
          }

          mapWareHouse.fitBounds(boundsWareHouse);
          mapWareHouse.setZoom(15);
        });
      }
</script>
@endsection