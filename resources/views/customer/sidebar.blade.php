<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{Asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ $username }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="{{ Request::route()->getName() ==  'customer.orders.list' ? 'active' : '' }} 
                 {{ Request::route()->getName() ==  'customer.orders.create' ? 'active' : '' }} 
                 {{ Request::route()->getName() ==  'customer.orders.getEdit' ? 'active' : '' }} 
                 {{ Request::route()->getName() ==  'customer.orders.historyOrders' ? 'active' : '' }} treeview">
        <a href="#">
          <i class="fa fa-laptop"></i>
          <span>Manager Order</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Request::route()->getName() ==  'customer.orders.list' ? 'active' : '' }}"><a href="{!! URL::route('customer.orders.list') !!}"><i class="fa fa-circle-o "></i>List Order</a></li>
          <li class="{{ Request::route()->getName() ==  'customer.orders.create' ? 'active' : '' }}"><a href="{!! URL::route('customer.orders.create') !!}"><i class="fa fa-circle-o"></i>Add Order</a></li>
          <li class="{{ Request::route()->getName() ==  'customer.orders.getEdit' ? 'active' : 'disabled' }}"><a href="#"><i class="fa fa-circle-o"></i>Edit Order</a></li>
          <li class="{{ Request::route()->getName() ==  'customer.orders.historyOrders' ? 'active' : '' }}" ><a href="{!! URL::route('customer.orders.historyOrders') !!}"><i class="fa fa-circle-o"></i>History Orders</a></li>
        </ul>
      </li>
      <li class="{{ Request::route()->getName() ==  'customer.shippers.listShippers' ? 'active' : '' }} treeview">
        <a href="{!! URL::route('customer.shippers.listShippers') !!}">
          <i class="fa fa-star-half-o"></i>
          <span>Shipper</span>
        </a>
      </li>
      <li class="treeview {{ Request::route()->getName() ==  'customer.account.getEdit' ? 'active' : '' }}">
        <a href="{{ URL::route('customer.account.getEdit', Auth::user()->id) }}">
          <i class="fa fa-user"></i>
          <span>Acount</span>
        </a>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
@section('script_pusher')
<style type="text/css">
    li.disabled {
        pointer-events: none;
        cursor: default;
    }
</style>
<script>
      Notification.requestPermission();
    var pusher = new Pusher(
        '72f8c2eed83c7dd0f357',
        {
          encrypted: true
        }
      );

      var channel = pusher.subscribe('receive-channel-' + {{ Auth::user()->id }} );
      channel.bind('receive-event-' + {{ Auth::user()->id }}, function(data) {

          var total = $(".dropdown-toggle p").html();
          document.getElementById('total').innerHTML = parseInt(total) + 1;

          var url = '{{ URL::route("customer.orders.confirm", "") }}' + '/' + data.id;
          $("ul #notify").append(
            '<li><a href="" onclick="return (window.confirm(\'Are you want to confirm?\') ? true : false)"><i class="fa fa-truck text-aqua"></i> Order(' + data.name + ', id: ' + data.id + ' ) were receive.</a></li>');

        // Desktop Notification
          var e = new Notification("Pickup Order", {
            body: data.msg + "\n" + data.weapon,
            icon: "http://icons.iconarchive.com/icons/graphics-vibe/hot-burning-social/128/facebook-icon.png",
            tag: "MR-THHUYEN"
          });
          e.onclick = function() {
            location.href = "http://127.0.0.1:8000/customer/orders/list";
          }
          e.onshow = function() {
            console.info("Mr-thuyen");
          }
          e.onclose = function() {
            console.info("Close");
          }
      });

      //pickup order
      var channelPickup = pusher.subscribe('pickup-channel-' + {{ Auth::user()->id }} );
      channelPickup.bind('pickup-event-' + {{ Auth::user()->id }}, function(data) {

          var total = $(".dropdown-toggle p").html();
          document.getElementById('total').innerHTML = parseInt(total) + 1;

          var url = '{{ URL::route("customer.orders.confirm", "") }}' + '/' + data.id;
          $("ul #notify").append(
            '<li><a href="' + url +'" onclick="return (window.confirm(\'Are you want to confirm?\') ? true : false)"><i class="fa fa-truck text-aqua"></i> Order(' + data.name + ', id: ' + data.id + ' ) were pickup.</a></li>');

        // Desktop Notification
          var e = new Notification("Receive", {
            body: data.msg + "\n" + data.weapon,
            icon: "http://icons.iconarchive.com/icons/graphics-vibe/hot-burning-social/128/facebook-icon.png",
            tag: "MR-THHUYEN"
          });
          e.onclick = function() {
            location.href = "http://127.0.0.1:8000/customer/orders/list";
          }
          e.onshow = function() {
            console.info("Mr-thuyen");
          }
          e.onclose = function() {
            console.info("Close");
          }
      });


      // delivery
      var channelDelivery = pusher.subscribe('delivery-channel-' + {{ Auth::user()->id }} );
      channelDelivery.bind('delivery-event-' + {{ Auth::user()->id }}, function(data) {

          var total = $(".dropdown-toggle p").html();
          document.getElementById('total').innerHTML = parseInt(total) + 1;

           var url = '{{ URL::route("customer.orders.confirm", "") }}' + '/' + data.id;
          $("ul #notify").append(
            '<li><a href="' + url +'" onclick="return (window.confirm(\'Are you want to confirm?\') ? true : false)"><i class="fa fa-truck text-aqua"></i> Order(' + data.name + ', id: ' + data.id + ' ) were delivered.</a></li>');

        // Desktop Notification
          var e = new Notification("Delivery", {
            body: data.msg + "\n" + data.weapon,
            icon: "http://icons.iconarchive.com/icons/graphics-vibe/hot-burning-social/128/facebook-icon.png",
            tag: "MR-THHUYEN"
          });
          e.onclick = function() {
            location.href = "http://127.0.0.1:8000/customer/orders/list";
          }
          e.onshow = function() {
            console.info("Mr-thuyen");
          }
          e.onclose = function() {
            console.info("Close");
          }
      });


</script>
@endsection