<header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>BShip</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- search form -->
        <form action="#" method="get" class="col-sm-6">
            <div id="custom-search-input" style="padding-top: 2%;">
                <div class="input-group col-md-12">
                    <input type="text" class="  search-query form-control" placeholder="Search" />
                    <span class="input-group-btn">
                        <button class="btn btn-danger" type="button">
                            <span class=" glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </div>
        </form>
        <!-- /.search form -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                            @if(isset($deliveries) and isset($receives))
                                <?php $total = $deliveries->count() + $receives->count() ?>
                                @else
                                <?php $total = 0 ?>
                            @endif
                        <p class="label label-warning" id="total" value="{{ $total }}">{{ $total }}</p>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu" id="notify">
                                @if(isset($receives))
                                    @foreach($receives as $receive)
                                        <li>
                                            <a href="{!! URL::route('customer.orders.confirm', $receive->id) !!}" onclick="return (window.confirm('Are you want to confirm?') ? true : false)">
                                                <i class="fa fa-truck text-aqua"></i> Order( {{ $receive->name }}, id = {{ $receive->id }}) were received.
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                                @if(isset($deliveries))
                                @foreach($deliveries as $delivery)
                                    <li>
                                        <a href="{!! URL::route('customer.orders.confirm', $delivery->id) !!}" onclick="return (window.confirm('Are you want to confirm?') ? true : false)">
                                            <i class="fa fa-flag text-aqua"></i> Order( {{ $delivery->name }}, id = {{ $delivery->id }}) has been successfully delivered.
                                        </a>
                                    </li>
                                @endforeach
                                @endif
                            </ul>
                        </li>
                        <li class="footer"><a href="{!! URL::route('customer.orders.confirmOrders') !!}">View all</a></li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{Asset('dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ $username }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{Asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                            <p>
                                Alexander Pierce - Web Developer
                                <small>Member since Nov. 2012</small>
                            </p>
                        </li>
                      <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ $link }}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{!! url('logout') !!}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                  <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>