@extends('layouts.master')
@section('title', trans('Shipper'))
@section('header')
@include('layouts.header')
@endsection
@section('sidebar')
@include('shipper.sidebar')
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Information
      <small>order</small>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- table -->
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped display"cellspacing="0">
          <thead>
            <tr>
              <th>ID</th>
              <th>Status</th>
              <th>Type</th>
              <th>Ware House</th>
              <th>Recipient</th>
              <th>Time</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>234
              </td>
              <td>Đã nhận hàng</td>
              <td>2</td>
              <td>W1</td>
              <td>Recipient1</td>
              <td>11/10/2016</td>
              <td>
                <button class="btn btn-success btn-edit">Direct</button>
              </td>
            </tr>
            <tr>
              <td>234
              </td>
              <td>Giao hàng thành công</td>
              <td>2</td>
              <td>W2</td>
              <td>Recipient1</td>
              <td>11/10/2016</td>
              <td>
                <button class="btn btn-success btn-edit">Direct</button>
              </td>
            </tr>
            <tr>
              <td>234
              </td>
              <td>Đã nhận hàng</td>
              <td>2</td>
              <td>W3</td>
              <td>Recipient1</td>
              <td>11/10/2016</td>
              <td>
                <button class="btn btn-success btn-edit">Direct</button>
              </td>
            </tr><tr>
            <td>234
            </td>
            <td>Đã nhận hàng</td>
            <td>2</td>
            <td>W4</td>
            <td>Recipient3</td>
            <td>11/10/2016</td>
            <td>
              <button class="btn btn-success btn-edit">Direct</button>
              <button class="btn btn-primary">Confirm</button>
            </td>
          </tr>

        </tbody>
        <tfoot>
          <tr>
            <th>ID</th>
            <th>Status</th>
            <th>Type</th>
            <th>Ware House</th>
            <th>Recipient</th>
            <th>Time</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>

  <!-- end table -->

  <!-- /.row -->

</section>
<!-- /.content -->
</div>
@endsection