@extends('layouts.master')
@section('title', trans('Shipper'))
@section('header')
@include('layouts.header')
@endsection
@section('sidebar')
@include('shipper.sidebar')
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Edit
      <small>Account</small>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
<form>
  <!-- người nhận -->
  <fieldset class="form-group">
    <div> &nbsp;</div>
    <div class="form-group">
      <p>Phone</p>
      <input type="text" class="form-control" placeholder="Phone">
    </div>
    <div class="form-group">
      <p>Họ tên</p>
      <input type="text" class="form-control"  placeholder="Name">
    </div>
  <div class="form-group">
      <p>Address</p>
      <input type="text" class="form-control"  placeholder="Address">
  </div>
  <div class="form-group">
      <p>Password</p>
      <input name="inputPassword" type="password" class="form-control"  placeholder="******">
  </div>
  <div class="form-group">
      <p>Password Confirm</p>
      <input name="inputPasswordConfirm" type="password" class="form-control"  placeholder="******">
  </div>
  </fieldset>
  <!-- end người nhận -->

  <button type="submit" class="btn btn-primary">Change</button>
</form>



</section>
<!-- /.content -->
</div>
@endsection