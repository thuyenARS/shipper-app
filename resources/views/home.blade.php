<!DOCTYPE html>
<html>
  <head>
    <title>Talking with Pusher</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <div class="content">
        <h1>Laravel 5 and Pusher is fun!</h1>
        <ul id="messages" class="list-group">
        </ul>
      </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://js.pusher.com/3.1/pusher.min.js"></script>
    <script>
      //instantiate a Pusher object with our Credential's key
      var pusher = new Pusher(
        '72f8c2eed83c7dd0f357',
        {
          encrypted: true
        }
      );
      var channel = pusher.subscribe('test-channel');
      Notification.requestPermission();
      channel.bind('test-event', function(data) {
          var e = new Notification("Test", {
            body: data.msg + "\n" + data.weapon,
            icon: "http://icons.iconarchive.com/icons/graphics-vibe/hot-burning-social/128/facebook-icon.png",
            tag: "MR-THHUYEN"
          });
          e.onclick = function() {
            location.href = "http://127.0.0.1:8000/bridgeabc";
          }
          e.onshow = function() {
            console.info("Mr-thuyen");
          }
          e.onclose = function() {
            console.info("Close");
          }
      });
    </script>
  </body>
</html>