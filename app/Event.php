<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'summary'
    ];

    /**
     * Create and update new event
     *
     * @param  $event
     * @return array
     */
    public static function createEvent($request)
    {
        $event = new Event();
        return $event->create($request);
    }

    /**
     * List Customers
     *
     * @param  $event, int $shop_id
     * @return array
     */
    public static function listEvent()
    {
        $events = Event::all();
        return $events;      
    }

    /**
     * Edit Shipper
     *
     * @param int $id, array $request
     * @return array
     */
    public static function updateEvent($id, $request)
    {
        $event = Event::find($id);
        if ($request == null) {
            return $event;
        } else {
            return $event->update($request);
        }
    }

    /**
     * Delete User
     *
     * @param $id
     * @return array
     */
    public static function deleteEvent($id)
    {
        $event = Event::find($id);
        return $event->delete();       
    }
}
