<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use App\Customer;
use DB;
use Input;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'email', 'address', 'password', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at'
    ];

    /**
     * Get the customer of user.
     */
    public function customer()
    {
        return $this->hasOne('App\Customer');
    }

    /**
     * Get the customer of user.
     */
    public function shipper()
    {
        return $this->hasOne('App\Shipper');
    }

    /**
     * List User
     * @param string $key
     * @return array
     */
    public static function listUser($id, $type)
    {
        if ($id != null ) {
            $users = User::find($id);
        } elseif ($type != null) {
            $users = User::where('type', '=', $type)->get();
        } else {
            $users = User::get();
        }
        return $users;
    }

    /**
     * Create new User
     *
     * @param  array  $request
     * @return array
     */
    public static function createUser($request, $type)
    {
        DB::beginTransaction();
        $user = new User();
        $request['password'] = Hash::make($request['password']);
        $request['type'] = $type;
        $createUser = $user->create($request);
        if (empty($createUser)) {
            return 0;
        } 
        if ($type == 2) {
            $request['user_id'] = $createUser['id'];
            $customer = Customer::createCustomer($request);
            if (empty($customer)) {
                DB::rollBack();
                return 0;
            } else {
                DB::commit();
                return 1;
            }
        }
        if ($type == 3) {
            $request['user_id'] = $createUser['id'];
            $shipper = Shipper::createShipper($request);
            if (empty($shipper)) {
                DB::rollBack();
                return 0;
            } else {
                DB::commit();
                return 1;
            }
        }
        DB::commit();
        return 1;
    }

    /**
     * Edit User
     *
     * @param int $id, array $request
     * @return array
     */
    public static function updateUser($id, $request)
    {
        $user = User::find($id);
        if ($request == null) {
            return $user;
        } else {
            if ($request['password'] == null) {
                $request['password'] = $user->password;
            } else {
                $request['password'] = Hash::make($request['password']);
            }
            return $user->update($request);
        }
    }

    /**
     * Delete User
     *
     * @param $id
     * @return array
     */
    public static function deleteUser($id)
    {
        $user = User::find($id);
        return $user->delete();       
    }

    /**
     * Info User
     *
     * @param $id
     * @return array
     */
    public static function info($id)
    {
        $user = User::find($id);
        return $user;       
    }
}
