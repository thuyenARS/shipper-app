<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;
use App\User;
use App\Shipper;

class Rating extends Model
{
    protected $table = 'ratings';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shipper_id', 'customer_id', 'rate'
    ];

    /**
     * Get the shipper of rating.
     */
    public function shipper()
    {
        return $this->hasOne('App\Shipper');
    }

    /**
     * Get the customer of rating.
     */
    public function customer()
    {
        return $this->hasOne('App\Customer');
    }
}
