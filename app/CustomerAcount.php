<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAcount extends Model
{
    protected $table = 'customer_acounts';

    public $timestamps = false;
}
