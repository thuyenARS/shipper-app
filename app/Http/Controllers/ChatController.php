<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChatMessage;
use App\Http\Requests;

class ChatController extends Controller
{
    public function testabc($username)
	{
		// $tests = ChatMessage::get();
		// return view('user.booktours',compact('tests'));
		$tests = ChatMessage::get();
		return view('chat',compact('username', 'tests'));
	}
	public function store(Request $request)
	{
		$message = new ChatMessage();
		$message ->msg = $request['msg'];
		$message->save();
	}

	public function ajax()
	{
		ini_set('max_execution_time', 7200);
		while (ChatMessage::where('check', null)->count() < 1) {
			usleep(1000);
		}
		if (ChatMessage::where('check', null)->count() > 0 )
		{
			$data = ChatMessage::where('check', null)->first();
			$id = $data ->id;
			$msg = $data->msg;
			$edit = ChatMessage::find($id);
			$edit->check = 1;
			$edit->save();
			return $msg;
			// return Response()->json([
			// 		'msg' => $data->msg
			// 	]);
		}
		return "axa";	
	}
}
