<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $redirectAfterLogout = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login(LoginRequest $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        if ($this->auth->attempt(['email' => $email, 'password' => $password])) {
            if (Auth::user()->type == 0 || Auth::user()->type == 1) {
                return redirect()->route('admin.admin.list');
            } elseif (Auth::user()->type == 2) {
                return redirect()->route('customer.orders.list');                
            }
        } else {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => trans('Email or Password not found.')]);
        }
    }

    public function loginApp(LoginRequest $request)
    {
        $email = $request->input('username');
        $password = $request->input('password');
        $token = csrf_token();
        $token = "\"token\":" + $token;
        // if ($this->auth->attempt(['email' => $email, 'password' => $password])) {
            return $token;
        // } else {
            // return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => trans('Email or Password not found.')]);
        // }
    }

    /**
     * Log the user out of the application.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/login');
    }
}
