<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Requests\EditUserRequest;
use App\User;

class UserController extends Controller
{
    public function getCreate() 
    {
        return view('admin.admin.create');
    }

    public function postCreate(UserRequest $request) 
    {
        $type = 1;
        $user = User::createUser($request->all(), $type);
        if ($user == 1) {
            return redirect()->route('admin.admin.list')->with(['flash_level' => 'success', 'flash_message' => trans('Create new Users successfull')]);
        } else {
            return redirect()->route('admin.admin.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Create new Users fail')]);
        }
    }

    /**
     * Show the application list user screen to the user. 
     *
     * @param array $request
     * @return Response
     */
    public function list() 
    {
        $id =null;
        $type = 1;
        $users = User::listUser($id, $type);
        return view('admin.admin.list', compact('users'));
    }

    public function getEdit($id) 
    {
        $user = User::updateUser($id, null);
        return view('admin.admin.edit', compact('user', 'id'));
    }

    /**
     * Post data to create a new user. 
     *
     * @param array  $request, int $id
     * @return Response
     */
    public function postEdit($id, EditUserRequest $request)
    {
        $user = User::updateUser($id, $request->all());
        if (!empty($user)) {
            return redirect()->route('admin.admin.list')->with(['flash_level' => 'success', 'flash_message' => trans('Editing user successfull')]);
        } else {
            return redirect()->route('admin.admin.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Editing the user failed.')]);
        }
    }

    /**
     * Show the application delete user screen to the customer. 
     *
     * @param array $request, int $id
     * @return Response
     */
    public function delete($id) 
    {
        $delete = User::deleteUser($id);
        if ($delete == true) {
            return redirect()->route('admin.list')->with(['flash_level' => 'success', 'flash_message' => trans('User was deleted.')]);            
        } else {
            return redirect()->route('admin.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Failed to delete user.')]);
        }
    }
}
