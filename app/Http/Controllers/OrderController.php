<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Order;
use App\WareHouse;
use App\Shipper;
use App\User;
use App\Recipients;
use App\Commodity;
use App\Customer;
use App\Rating;
use Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\App;

class OrderController extends Controller
{
    public function getCreate() 
    {
        $id = Auth::user()->id;
        $orders = Order::listOrder($id);
        $customer_id = Customer::select('id')->where('user_id', $id)->first();
        $ware_houses = WareHouse::listWareHouse($customer_id->id);
        $receives = $orders->where('status', '=', 3);
        $deliveries = $orders->where('status', '=', 5);
        return view('customer.create_order', compact('ware_houses', 'receives', 'deliveries'));
    }

    // rating
    public function rating(Request $request)
    {
        $rating = Order::rating($request);
        if (!empty($rating)) {
        //     return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => trans('Rating successfull')]);
            $pusher = App::make('pusher');
            $pusher->notify(
              array("kittens"),
              array(
                'fcm' => array(
                  'data' => array(
                    'title' => 'Confirm success order!' . $request->rate,
                    'icon' => 'androidlogo'
                  ),
                ),
                'webhook_url' => 'https://example.com/endpoint',
                'webhook_level' => 'INFO',
             )
            );
        }
        // return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => trans('Rating error')]);
    }

    // API return list orders
    public function getShipOrder() 
    {
        $list = Order::listShipOrder();
        return Response::json($list, 200);
    }

    // API return list orders
    public function ordersDetail($id) 
    {
        $order = Order::ordersDetail($id);
        return Response::json($order, 200);
    }

    // API return list received orders
    public function getReceivedOrders() 
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        $list = Order::listReceivedOrders($user->id);
        // if (empty($list)) {
        //     return Response::json(array(), 200);
        // }
        return Response::json($list, 200);
    }

    // API return orders nearest Shipper
    public function getOrderNearest($lat, $lng, $radius) 
    {
        $list = Order::getOrderNearest($lat, $lng, $radius);
        return Response::json($list, 200);
    }

    // API return orders nearest Shipper
    public function getOrderNearestToOrder($latRecipient, $lngRecipient, $latWareHouse, $lngWareHouse) 
    {
        $list = Order::getOrderNearestToOrder($latRecipient, $lngRecipient, $latWareHouse, $lngWareHouse);
        return Response::json($list, 200);
    }

    public function postCreate(OrderRequest $request) 
    {
        $id = Auth::user()->id;
        $customer_id = Customer::select('id')->where('user_id', $id)->first();
        $order = Order::createOrder($request->all(), $customer_id->id);
        if ($order == 1) {
            return redirect()->route('customer.orders.list')->with(['flash_level' => 'success', 'flash_message' => trans('Create new Order successfull')]);
        } else {
            return redirect()->route('customer.orders.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Create new Order fail')]);
        }
    }

    /**
     * Show the application list order screen to the order. 
     *
     * @return Response
     */
    public function list() 
    {
        $id = Auth::user()->id;
        $orders = Order::listOrder($id);
        foreach ($orders as $order) {            
            $shipper = Shipper::find($order['shipper_id']);
            if (empty($shipper)) {
                $nameShipper[] = null;
            } else {
                $user = User::where('id', '=', $shipper->user_id)->first();
                $nameShipper[] = $user->name;
            }
        }
        $wating = $orders->where('status', '=', 1);
        $receives = $orders->where('status', '=', 3);
        $deliveries = $orders->where('status', '=', 5);
        $success = $orders->where('status', '=', 6);
        return view('customer.index', compact('orders', 'nameShipper', 'receives', 'deliveries', 'wating', 'success'));
    }

    /**
     * Show the application list order screen to the order. 
     *
     * @return Response
     */
    public function historyOrders() 
    {
        $id = Auth::user()->id;
        $orders = Order::listOrder($id);
        $wating = $orders->where('status', '=', 1);
        $receives = $orders->where('status', '=', 3);
        $deliveries = $orders->where('status', '=', 5);
        $success = $orders->where('status', '=', 6);
        foreach ($success as $order) {            
            $shipper = Shipper::find($order['shipper_id']);
            if (empty($shipper)) {
                $nameShipper[] = null;
            } else {
                $user = User::where('id', '=', $shipper->user_id)->first();
                $nameShipper[] = $user->name;
            }
        }     
        return view('customer.history', compact('orders', 'nameShipper', 'receives', 'deliveries', 'wating', 'success'));
    }

    /**
     * Show the application list confirm order screen to the order. 
     *
     * @return Response
     */
    public function confirmOrders() 
    {
        $id = Auth::user()->id;
        $orders = Order::listOrder($id);

        $receives = $orders->where('status', '=', 3);
        foreach ($receives as $order) {            
            $shipper = Shipper::find($order['shipper_id']);
            if (empty($shipper)) {
                $nameShipperReceives[] = null;
            } else {
                $user = User::where('id', '=', $shipper->user_id)->first();
                $nameShipperReceives[] = $user->name;
            }
        }

        $deliveries = $orders->where('status', '=', 5);
        foreach ($deliveries as $order) {            
            $shipper = Shipper::find($order['shipper_id']);
            if (empty($shipper)) {
                $nameShipperDeliveries[] = null;
            } else {
                $user = User::where('id', '=', $shipper->user_id)->first();
                $nameShipperDeliveries[] = $user->name;
            }
        }

        return view('customer.confirm_orders', compact('orders', 'nameShipperReceives', 'nameShipperDeliveries', 'receives', 'deliveries'));
    }

    /**
     * Show the application list order screen to the order. 
     *
     * @return Response
     */
    public function listAllOrder() 
    {
        $orders = Order::listAllOrder();
        return view('admin.orders.index', compact('orders'));
    }

    public function getEdit($id) 
    {
        $idUser = Auth::user()->id;
        $orders = Order::listOrder($idUser);
        $receives = $orders->where('status', '=', 3);
        $deliveries = $orders->where('status', '=', 5);

        $customer_id = Customer::select('id')->where('user_id', $idUser)->first();
        $idCustomer = $customer_id->id;
        $ware_houses = WareHouse::listWareHouse($idCustomer);
        $recipient = Recipients::where('order_id', '=', $id)->first();
        $commodity = Commodity::where('order_id', '=', $id)->first();
        $order = Order::updateOrder($id, $idCustomer, null);
        return view('customer.edit', compact('order', 'id', 'ware_houses', 'recipient', 'commodity', 'receives', 'deliveries'));
    }

    /**
     * Post data to update a new order. 
     *
     * @param array  $request, int $id
     * @return Response
     */
    public function postEdit($id, OrderRequest $request)
    {
        $idUser = Auth::user()->id;
        $customer_id = Customer::select('id')->where('user_id', $idUser)->first();
        $idCustomer = $customer_id->id;
        $order = Order::updateOrder($id, $idCustomer, $request->all());
        if (!empty($order)) {
            return redirect()->route('customer.orders.list')->with(['flash_level' => 'success', 'flash_message' => trans('Editing order successfull')]);
        } else {
            return redirect()->route('customer.orders.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Editing the order failed.')]);
        }
    }

    /**
     * Show the application delete order screen to the order. 
     *
     * @param array $request, int $id
     * @return Response
     */
    public function delete($id) 
    {
        $delete = Order::deleteOrder($id);
        if ($delete == true) {
            return redirect()->route('customer.orders.list')->with(['flash_level' => 'success', 'flash_message' => trans('Order was deleted.')]);            
        } else {
            return redirect()->route('customer.orders.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Failed to delete order.')]);
        }
    }

    /**
     * Show the application delete order screen to the order. 
     *
     * @param array $request, int $id
     * @return Response
     */
    public function deleteShipper($id) 
    {
        $delete = Order::deleteShipper($id);
        if ($delete == true) {
            return redirect()->route('admin.orders.list')->with(['flash_level' => 'success', 'flash_message' => trans('Shipper was remove.')]);            
        } else {
            return redirect()->route('admin.orders.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Failed to remove order.')]);
        }
    }

    /**
     * Show the application delete order screen to the order. 
     *
     * @param array $request, int $id
     * @return Response
     */
    public function cancelOrder($id) 
    {
        $delete = Order::deleteShipper($id);
        $order = Order::find($id);
        if ($delete == true) {
            $pusher = App::make('pusher'); 
            $pusher->trigger( 'receive-channel-' .  $order->customer->users->id,
                              'receive-event-' . $order->customer->users->id, 
                              array('msg' => 'The Shipper has cancel order (Order id: ' . $id . ") .",
                                    'weapon' => 'Bach Khoa Ha Noi'
                                ));
            return Response::json(['success' => 'Success'], 200);            
        } else {
            return Response::json(['error' => 'error'], 404); 
        }
    }

    /**
     * Show the application confirm order screen to the order. 
     *
     * @param array $request, int $id
     * @return Response
     */
    public function confirm($id) 
    {
        $confirm = Order::confirmOrder($id);
        if ($confirm == 1 ) {
            $pusher = App::make('pusher');
            $pusher->notify(
              array("kittens"),
              array(
                'fcm' => array(
                  'data' => array(
                    'title' => 'Confirm pickup order!',
                    'icon' => 'androidlogo'
                  ),
                ),
                'webhook_url' => 'https://example.com/endpoint',
                'webhook_level' => 'INFO',
             )
            );
            return redirect()->route('customer.orders.list')->with(['flash_level' => 'success', 'flash_message' => trans('Confirm order success.')]);
        } elseif ( $confirm == 2) {
            $pusher = App::make('pusher');
            $pusher->notify(
              array("kittens"),
              array(
                'fcm' => array(
                  'data' => array(
                    'title' => 'Confirm success order!',
                    'icon' => 'androidlogo'
                  ),
                ),
                'webhook_url' => 'https://example.com/endpoint',
                'webhook_level' => 'INFO',
             )
            );
             return redirect()->route('customer.orders.list')->with(['flash_level' => 'success', 'flash_message' => trans('Confirm order success.')]);
        }
            return redirect()->route('customer.orders.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Confirm order false.')]);
    }

    //API ReveiceOrder
    public function receveOrders($id)
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        $shipper = Shipper::select('id')->where('user_id', $user->id)->first();
        $receive = Order::receveOrders($id, $shipper->id);
        $order = Order::where('id', '=', $id)->with('customer.users')->first();
        if ($receive == 1) {
            $pusher = App::make('pusher'); 
            $pusher->trigger( 'receive-channel-' .  $order->customer->users->id,
                              'receive-event-' . $order->customer->users->id, 
                              array('msg' => 'The Shipper has recieve order (Order id: ' . $id . ") .",
                                    'weapon' => 'Bach Khoa Ha Noi',
                                    'name' => $order->name,
                                    'id' => $id
                                ));
            return Response::json(['success' => 'Success'], 200);
        } elseif ($receive == 2) {
            return Response::json(['error' => 'Not enough money'], 402);
        } elseif ($receive == 3) {
            return Response::json(['error' => 'False'], 404);
        }
        return Response::json(['error' => 'False'], 403);
    }

    //API ReveiceOrder
    public function pickupOrders($id)
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        $shipper = Shipper::select('id')->where('user_id', $user->id)->first();
        $pickup = Order::pickupOrders($id, $shipper->id);
        $order = Order::where('id', '=', $id)->with('customer.users')->first();
        if (!empty($pickup)) {
            $pusher = App::make('pusher'); 
            $pusher->trigger( 'pickup-channel-' .  $order->customer->users->id,
                              'pickup-event-' . $order->customer->users->id, 
                              array('msg' => 'The Shipper has pickup order (Order id: ' . $id . ") .",
                                    'weapon' => 'Bach Khoa Ha Noi',
                                    'name' => $order->name,
                                    'id' => $id
                                ));
            return Response::json(['success' => 'Success'], 200);
        } 
        return Response::json(['error' => 'Error'], 404);
    }

    //API ReveiceOrder
    public function deliveryOrders($id)
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }
        $shipper = Shipper::select('id')->where('user_id', $user->id)->first();
        $delivery = Order::deliveryOrders($id, $shipper->id);
        $order = Order::where('id', '=', $id)->with('customer.users')->first();
        if (!empty($delivery)) {
            $pusher = App::make('pusher'); 
            $pusher->trigger( 'delivery-channel-' .  $order->customer->users->id,
                              'delivery-event-' . $order->customer->users->id, 
                              array('msg' => 'The order has been successfully delivered (Order id: ' . $id . ") .",
                                    'weapon' => 'Bach Khoa Ha Noi',
                                    'name' => $order->name,
                                    'id' => $id
                                ));
        }
    }
}
