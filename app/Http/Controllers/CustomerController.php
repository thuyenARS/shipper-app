<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Acount;
use App\Customer;
use App\Shipper;
use App\Http\Requests;
use App\Http\Requests\EditCustomerRequest;
use App\Http\Requests\CustomerRequest;

class CustomerController extends Controller
{
    public function getCreate() 
    {
    	return view('admin.customers.create');
    }

    public function postCreate(CustomerRequest $request) 
    {
    	$type = 2;
    	$acount = Acount::checkAcount($request['acount_id']);
    	if (!empty($acount)) {
    		$user = User::createUser($request->all(), $type);
	    	if ($user == 1) {

	            return redirect()->route('admin.admin.list')->with(['flash_level' => 'success', 'flash_message' => trans('Create new Users successfull')]);
	        } else {
	            return redirect()->route('admin.admin.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Create new Users fail')]);
	        }
    	} else {
    		return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => trans('Acount does not exist')]);
    	}
    	
    }

    /**
     * Show the application list user screen to the user. 
     *
     * @return Response
     */
    public function list() 
    {
        $customers = Customer::listCustomer();
        // echo $customers;
        return view('admin.customers.list', compact('customers'));
    }

    /**
     * List Shippers
     *
     * @param  $event, int $shop_id
     * @return array
     */
    public static function listShippers()
    {
        $shippers = Shipper::listShipper();
        return view('customer.list-shipper', compact('shippers'));
    }

    public function getEdit($id) 
    {
        $customer = User::updateUser($id, null);
        return view('admin.customers.edit', compact('customer', 'id'));
    }

    public function getEditAccount($id) 
    {
        $customer = User::updateUser($id, null);
        return view('customer.edit_acount', compact('customer', 'id'));
    }

    /**
     * Post data to update a new customer. 
     *
     * @param array  $request, int $id
     * @return Response
     */
    public function postEdit($id, EditCustomerRequest $request)
    {
        $customer = Customer::updateCustomer($id, $request->all());
        if (!empty($customer)) {
            return redirect()->route('admin.customers.list')->with(['flash_level' => 'success', 'flash_message' => trans('Editing user successfull')]);
        } else {
            return redirect()->route('admin.customers.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Editing the user failed.')]);
        }
    }

    /**
     * Post data to update a new customer. 
     *
     * @param array  $request, int $id
     * @return Response
     */
    public function postEditAccount($id, EditCustomerRequest $request)
    {
        $customer = Customer::updateCustomer($id, $request->all());
        if (!empty($customer)) {
            return redirect()->route('customer.orders.list')->with(['flash_level' => 'success', 'flash_message' => trans('Editing account successfull')]);
        } else {
            return redirect()->route('customer.orders.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Editing the account failed.')]);
        }
    }

    /**
     * Show the application delete user screen to the user. 
     *
     * @param array $request, int $id
     * @return Response
     */
    public function delete($id) 
    {
        $delete = User::deleteUser($id);
        if ($delete == true) {
            return redirect()->route('admin.customers.list')->with(['flash_level' => 'success', 'flash_message' => trans('User was deleted.')]);            
        } else {
            return redirect()->route('admin.customers.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Failed to delete user.')]);
        }
    }

}
