<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;
use App\Http\Requests\EventRequest;
use App\Http\Requests;
use Response;

class EventController extends Controller
{
    public function getCreate() 
    {
    	return view('admin.events.create');
    }

    public function postCreate(EventRequest $request) 
    {
		$event = Event::createEvent($request->all());
    	if (!empty($event)) {
            return redirect()->route('admin.events.list')->with(['flash_level' => 'success', 'flash_message' => trans('Create new Event successfull')]);
        } else {
            return redirect()->route('admin.events.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Create new Event fail')]);
        }
    	
    }

    /**
     * Show the application list user screen to the user. 
     *
     * @return Response
     */
    public function list() 
    {
        $events = Event::listEvent();
        return view('admin.events.list', compact('events'));
    }

    /**
     * Show the application list user screen to the user. 
     *
     * @return Response
     */
    public function listEventForShipper() 
    {
        $events = Event::listEvent()->first();
        return Response::json($events, 200);
    }

    public function getEdit($id) 
    {
        $event = Event::updateEvent($id, null);
        return view('admin.events.edit', compact('event', 'id'));
    }

    /**
     * Post data to create a new shipper. 
     *
     * @param array  $request, int $id
     * @return Response
     */
    public function postEdit($id, EventRequest $request)
    {
        $event = Event::updateEvent($id, $request->all());
        if (!empty($event)) {
            return redirect()->route('admin.events.list')->with(['flash_level' => 'success', 'flash_message' => trans('Editing event successfull')]);
        } else {
            return redirect()->route('admin.events.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Editing the event failed.')]);
        }
    }

    /**
     * Show the application delete user screen to the user. 
     *
     * @param array $request, int $id
     * @return Response
     */
    public function delete($id) 
    {
        $delete = Event::deleteEvent($id);
        if ($delete == true) {
            return redirect()->route('admin.events.list')->with(['flash_level' => 'success', 'flash_message' => trans('Event was deleted.')]);            
        } else {
            return redirect()->route('admin.events.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Failed to delete event.')]);
        }
    }
}
