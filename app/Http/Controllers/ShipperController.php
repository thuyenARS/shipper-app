<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Acount;
use App\Customer;
use App\Shipper;
use Response;
use JWTAuth;
use App\Http\Requests\ShipperRequest;
use App\Http\Requests\EditShipperRequest;
use App\Http\Requests;

class ShipperController extends Controller
{
    public function getCreate() 
    {
    	return view('admin.shippers.create');
    }

    public function postCreate(ShipperRequest $request) 
    {
    	$type = 3;
    	$acount = Acount::checkAcount($request['acount_id']);
    	if (!empty($acount)) {
    		$user = User::createUser($request->all(), $type);
	    	if ($user == 1) {
	            return redirect()->route('admin.shippers.list')->with(['flash_level' => 'success', 'flash_message' => trans('Create new Users successfull')]);
	        } else {
	            return redirect()->route('admin.shippers.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Create new Users fail')]);
	        }
    	} else {
    		return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => trans('Acount does not exist')]);
    	}
    	
    }

    /**
     * Show the application list user screen to the user. 
     *
     * @return Response
     */
    public function list() 
    {
        $shippers = Shipper::listShipper();
        return view('admin.shippers.list', compact('shippers'));
    }

    /**
     * Show the application list user screen to the user. 
     *
     * @return Response
     */
    public function info() 
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        $infoShipper = Shipper::info($user->id);
        $infoUser = User::info($user->id);
        $info['name'] = $infoUser->name;
        $info['email'] = $infoUser->email;
        $info['address'] = $infoUser->address;
        $info['phone'] = $infoUser->phone;
        $info['level'] = $infoShipper->level;
        return Response::json($info, 200);
    }

    public function getEdit($id) 
    {
        $shipper = User::updateUser($id, null);
        return view('admin.shippers.edit', compact('shipper', 'id'));
    }

    /**
     * Post data to create a new shipper. 
     *
     * @param array  $request, int $id
     * @return Response
     */
    public function postEdit($id, EditShipperRequest $request)
    {
        $shipper = Shipper::updateShipper($id, $request->all());
        if (!empty($shipper)) {
            return redirect()->route('shippers.list')->with(['flash_level' => 'success', 'flash_message' => trans('Editing user successfull')]);
        } else {
            return redirect()->route('shippers.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Editing the user failed.')]);
        }
    }

    /**
     * Show the application delete user screen to the user. 
     *
     * @param array $request, int $id
     * @return Response
     */
    public function delete($id) 
    {
        $delete = User::deleteUser($id);
        if ($delete == true) {
            return redirect()->route('shippers.list')->with(['flash_level' => 'success', 'flash_message' => trans('User was deleted.')]);            
        } else {
            return redirect()->route('shippers.list')->with(['flash_level' => 'danger', 'flash_message' => trans('Failed to delete user.')]);
        }
    }
}
