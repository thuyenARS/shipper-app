<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Acount;
use App\Customer;
use Response;
use Validator;
use App\Http\Requests\CustomerRequest;

class MemberController extends Controller
{
    public function register() 
    {
        return view('auth.register');
    }

    public function postRegister(CustomerRequest $request) 
    {
        $type = 2;
        $acount = Acount::checkAcount($request['acount_id']);
        if (!empty($acount)) {
        $user = User::createUser($request->all(), $type);
        if ($user == 1) {
                return view('auth.login');
            } else {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => trans('Create new Users fail')]);
            }
        } else {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => trans('Acount does not exist')]);
        }
    }

    public function postRegisterShipper(Request $request) 
    {
         $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users,email|email'
        ]);
         if ($validator->fails()){
            return Response::json(['error' => 'Email have exist'], 403);
        }
        $type = 3;
        $acount = Acount::checkAcount($request['acount_id']);
        if (!empty($acount)) {
            $user = User::createUser($request->all(), $type);
            if (!empty($user)) {            
                return Response::json(['success' => 'Successfully'], 200);
            } 
            return Response::json(['error' => 'register false'], 404);
        }
        return Response::json(['error' => 'Account does not exist'], 401);
    }
}
