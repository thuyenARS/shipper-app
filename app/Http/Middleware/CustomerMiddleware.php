<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class CustomerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (Auth::user()->type == 2) {
                return $next($request);
            } else { 
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => trans('Permission denied.')]);
            }            
        } else {
            return redirect('/login');
        }
    }
}
