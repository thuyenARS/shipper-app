<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {     
        if (Auth::guard($guard)->check()) {
            if (Auth::user()->type == 0 || Auth::user()->type == 1) {
                return $next($request);
            } else { 
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => trans('Permission denied.')]);
            }            
        } else {
            return redirect('http://127.0.0.1:8000/login/');
        }
    }
}
