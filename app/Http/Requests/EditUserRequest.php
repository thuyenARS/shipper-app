<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $this->id,
            'password' => 'between:8,50|regex:/^([a-zA-Z]+)([0-9!$#%]*$)/|confirmed',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'  => trans('Please enter Username'),
            'email.required' => trans('Please enter Email'),
            'email.unique' => trans('Email Is Exists'),
            'email.email' => trans('Email is not format'),
            'password.regex' => trans('Password is not format (a-zA-Z, 0-9)'),
            'password.between' => trans('Password is not format (8 - 50)'),
            'password.confirmed' => 'Password confirmation has not been entered.',
        ];
    }
}
