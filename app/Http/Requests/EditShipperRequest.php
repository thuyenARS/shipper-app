<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditShipperRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'email' => 'required|email|unique:users,email,' . $this->id,
            'password' => 'between:8,50|regex:/^([a-zA-Z]+)([0-9!$#%]*$)/|confirmed',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'  => trans('Please enter name'),
            'phone.required'  => trans('Please enter phone'),
            'address.required'  => trans('Please enter address'),
            'email.required' => trans('Please enter email'),
            'email.unique' => trans('Email is already exist'),
            'email.email' => trans('Email don\'t match '),
            'password.regex' => trans('Password is not format'),
            'password.between' => trans('Password is not format'),
            'password.confirmed' => 'Password don\'t match ',
        ];
    }
}
