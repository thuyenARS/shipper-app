<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   => 'required',
            'phone'   => 'required',
            'email'   => 'required|unique:users,email|email',
            'address'   => 'required',
            'password'   => 'required|between:8,50|regex:/^([a-zA-Z]+)([0-9!$#%]*$)/',
            'password_confirm'     => 'required|same:password'
        ];
    }

    public function messages(){
        return[
            'name.required'  => 'Please enter Username',
            'phone.required'  => 'Please enter Phone',
            'email.required'  => 'Please enter Email',
            'email.unique'    =>'Email Is Exists',
            'email.email'    =>'Email is not format',
            'address.required'  => 'Please enter Address',
            'password.required'  => 'Please Enter Password',
            'password.between'  => 'Password is not format (8 - 50)',
            'password.regex'  => 'Password is not format (a-zA-Z, 0-9)',
            'password_confirm.required'    => 'Please Enter Re-Password',
            'password_confirm.same'    => 'Two password Don \'t Match'
        ];
    }
}
