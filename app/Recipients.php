<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipients extends Model
{
    protected $table = 'recipients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'address', 'latitude', 'longtitude', 'order_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function order() {
        return $this->belongsTo('App\Order');
    }

    /**
     * Update new Recipients
     *
     * @param  $request
     * @return array
     */
    public static function createRecipient($request)
    {
        $recipient = new Recipients();
        $request['name'] = $request['name_recip'];
        $request['phone'] = $request['phone_recip'];
        $request['address'] = $request['address_recip'];
        $request['latitude'] = $request['lat_recip'];
        $request['longtitude'] = $request['long_recip'];
        $request['region'] = "BN";
        // $request['region'] = $request['region_recip'];
        echo $request['order_id'];
        return $recipient->create($request);
    }

    /**
     * Update new Recipients
     *
     * @param  $request
     * @return array
     */
    public static function updateRecipient($request)
    {
        $recipient = Recipients::where('order_id', '=', $request['order_id']);
        $data['name'] = $request['name_recip'];
        $data['phone'] = $request['phone_recip'];
        $data['address'] = $request['address_recip'];
        $data['latitude'] = $request['lat_recip'];
        $data['longtitude'] = $request['long_recip'];
        // $request['region'] = $request['region_recip'];
        return $recipient->update($data);
    }
}
