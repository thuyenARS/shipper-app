<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;

class WareHouse extends Model
{
    protected $table = 'ware_houses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'address', 'latitude', 'longtitude', 'customer_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'customer_id'
    ];

    public function customers(){
        return $this->belongsTo('App\Customer');
    }

    public function order(){
        return $this->hasOne('App\Order');
    }

    /**
     * List Orders
     * @param string $key
     * @return array
     */
    public static function listWareHouse($id)
    {
        $ware_houses = WareHouse::select();
        $ware_houses = $ware_houses->get()->where('customer_id',  $id);
        return $ware_houses;
    }

    /**
     * create warehouse
     * @param string $request
     * @return array
     */
    public static function createWareHouse($request)
    {
        $ware_houses = new WareHouse();
        $request['name'] = $request['name_wh'];
        $request['phone'] = $request['phone_wh'];
        $request['address'] = $request['address_wh'];
        $request['latitude'] = $request['lat_wh'];
        $request['longtitude'] = $request['long_wh'];
        $createWareHouse = $ware_houses->create($request);
        return $createWareHouse;
    }
}
