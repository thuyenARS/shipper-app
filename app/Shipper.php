<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Acount;
use Hash;
class Shipper extends Model
{
    protected $table = 'shippers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'level', 'acount_id', 'user_id'
    ];

    public function users() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function account() {
        return $this->belongsTo('App\Acount', 'acount_id', 'id');
    }

    public function orders(){
        return $this->hasMany('App\Order');
    }

    /**
     * Create and update new Shipper
     *
     * @param  $event, int $shop_id
     * @return array
     */
    public static function createShipper($request)
    {
        $shipper = new Shipper();
        return $shipper->create($request);
    }

    /**
     * List Customers
     *
     * @param  $event, int $shop_id
     * @return array
     */
    public static function listShipper()
    {
        $shippers = Shipper::get();
        $i = 0;
        foreach ($shippers as $shipper) {
            $userOfShipper = User::listUser($shipper->user_id, null);
            $balance = Acount::find($shipper->acount_id);
            $infoShipper[$i]['id'] = $userOfShipper->id;
            $infoShipper[$i]['name'] = $userOfShipper->name;
            $infoShipper[$i]['phone'] = $userOfShipper->phone;
            $infoShipper[$i]['email'] = $userOfShipper->email;
            $infoShipper[$i]['address'] = $userOfShipper->address;
            $infoShipper[$i]['level'] = $shipper->level;
            $infoShipper[$i]['balances'] = $balance->balances;
            $i++;
        }
        if (!empty($infoShipper)) {
            return $infoShipper; 
        }
    }

    /**
     * Edit Shipper
     *
     * @param int $id, array $request
     * @return array
     */
    public static function updateShipper($id, $request)
    {
        $shipper = User::find($id);
        if ($request == null) {
            return $shipper;
        } else {
            if ($request['password'] == null) {
                $request['password'] = $shipper->password;
            } else {
                $request['password'] = Hash::make($request['password']);
            }
            return $shipper->update($request);
        }
    }

    /**
     * Info Shipper
     *
     * @param int $id, array $request
     * @return array
     */
    public static function info($id)
    {
        $shipper = Shipper::select('level')->where('user_id', '=', $id)->first();
        return $shipper;
    }


}
