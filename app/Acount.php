<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'balances'
    ];

    /**
     * Get the customer of account.
     */
    public function customer()
    {
        return $this->hasOne('App\Customer');
    }

    /**
     * Get the customer of account.
     */
    public function shipper()
    {
        return $this->hasOne('App\Shipper');
    }

    /**
     * Check Account exist
     * @param string $id
     * @return array
     */
    public static function checkAcount($id)
    {
        $account = Acount::find($id);
        return $account;
    }

    /**
     * Add money
     * @param string $id
     * @return array
     */
    public static function addMoney($id, $money)
    {
        $account = Acount::find($id);
        $data['balances'] = $account->balances + $money;
        return $account->update($data);
    } 

    /**
     * Sub money
     * @param string $id
     * @return array
     */
    public static function subMoney($id, $money)
    {
        $account = Acount::find($id);
        $data['balances'] = $account->balances - $money;
        return $account->update($data);
    }    
}
