<?php

namespace App;

use App\Recipients;
use App\Commodity;
use App\Customer;
use App\User;
use App\Shipper;
use App\Acount;
use Illuminate\Database\Eloquent\Model;

use DB;


class Order extends Model
{
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status', 'customer_id', 'shipper_id', 'type', 'ship_cost', 'price', 'ware_house_id','rate'
    ];


    /**
     * Get the commodities of order.
     */
    public function commodities()
    {
        return $this->hasOne('App\Commodity');
    }

    /**
     * Get the recipient of order.
     */
    public function recipient()
    {
        return $this->hasOne('App\Recipients');
    }

    /**
     * Get the recipient of order.
     */
    public function ware_house()
    {
        return $this->belongsTo('App\WareHouse', 'ware_house_id', 'id');
    }

    public function customer() {
        return $this->belongsTo('App\Customer', 'customer_id', 'id');
    }

    public function shipper() {
        return $this->belongsTo('App\Shipper', 'shipper_id', 'id');
    }

    /**
     * Create and new Order
     *
     * @param  $request, $id
     * @return boolean
     */
    public static function createOrder($request, $id)
    {
        $order = new Order();
        DB::beginTransaction();
        $request['customer_id'] = $id;
        $request['status'] = 1;
        if ($request['radio-stacked'] == 2) {

            $wareHouse = WareHouse::createWareHouse($request);
            $request['ware_house_id'] = $wareHouse->id;

            // address of warehouse
            $from = $request['address_wh'];                
        } else {
            // address of warehouse
            $from = WareHouse::find($request['ware_house_id'])->address;             
        }

        // address of recipient
        $to = $request['address_recip'];      

        // calculator ShipCost
        $request['ship_cost'] = Order::calculatorShipCost($from, $to);
        $data = $request;
        $data['price'] = $request['price-commodity'] * $request['count-commodity'];
        $createOrder = $order->create($data);
        if (empty($createOrder)) {
            DB::rollBack();
            return 0;
        } else {
            $request['order_id'] = $createOrder['id'];
            $recip = Recipients::createRecipient($request);
            $commodities = Commodity::createCommodity($request);
            DB::commit();
            return 1;
        }
        return 1;
    }

    /**
     * List Orders
     * @param string $key
     * @return array
     */
    public static function listOrder($id)
    {
        $customer = Customer::where('user_id', '=', $id)->first();
        $orders = Order::select('id', 'name', 'status', 'customer_id', 'shipper_id', 'type', 'ship_cost', 'price', 'ware_house_id','rate')->where('customer_id', '=', $customer->id)->with('recipient')->with('commodities')->with('ware_house')->get();
        return $orders;
    }

    /**
     * List Orders
     * @param string $key
     * @return array
     */
    public static function listAllOrder()
    {
        $orders = Order::select('id', 'name', 'status', 'customer_id', 'shipper_id', 'type', 'ship_cost', 'price', 'ware_house_id')->with('recipient')->with('commodities')->with('ware_house')->with('customer.users')->with('shipper.users')->get();
        return $orders;
    }

    /**
     * List Orders
     * @param string $key
     * @return array
     */
    public static function listShipOrder()
    {
        $orders = Order::select('id', 'name', 'status', 'customer_id', 'shipper_id', 'type', 'ship_cost', 'price', 'ware_house_id')
            ->where('status', '=', 1)
            ->with('recipient')
            ->with('commodities')
            ->with('ware_house')
            ->with('customer.users')
            ->paginate(10);
        return $orders;
    }

    /**
     * List Orders
     * @param string $key
     * @return array
     */
    public static function ordersDetail($id)
    {
        $order = Order::select('id', 'name', 'status', 'customer_id', 'shipper_id', 'type', 'ship_cost', 'price', 'ware_house_id')
            ->where('id', '=', $id)
            ->with('recipient')
            ->with('commodities')
            ->with('ware_house')
            ->with('customer.users')
            ->first();
        return $order;
    }

    /**
     * List Orders
     * @param string $key
     * @return array
     */
    public static function listReceivedOrders($id)
    {
        $shipper = Shipper::where('user_id', '=', $id)->first();
        $orders = Order::select('id', 'name', 'status', 'customer_id', 'shipper_id', 'type', 'ship_cost', 'price', 'ware_house_id')
            ->where('shipper_id', '=', $shipper->id)
            ->where('status', '!=', 6)
            ->with('recipient')
            ->with('commodities')
            ->with('ware_house')
            ->with('customer.users')->get();
        return $orders;
    }

    /**
     * Edit Order
     *
     * @param int $id, array $request
     * @return array
     */
    public static function updateOrder($id, $idCustomer, $request)
    {
        $order = Order::find($id);
        if ($request == null) {
            return $order;
        } else {
            $request['customer_id'] = $idCustomer;
            $request['order_id'] = $order['id'];
            if ($order->status == 1) {
                if ($request['radio-stacked'] == 2) {
                    $wareHouse = WareHouse::createWareHouse($request);
                    $request['ware_house_id'] = $wareHouse->id;
                    // address of warehouse
                    $from = $request['address_wh']; 
                } else {
                    // address of warehouse
                    $from = WareHouse::find($request['ware_house_id'])->address;             
                }

                // address of recipient
                $to = $request['address_recip'];      

                // calculator ShipCost
                $request['ship_cost'] = Order::calculatorShipCost($from, $to);
                $data = $request;
                $data['price'] = $request['price-commodity'] * $request['count-commodity'];
                $recip = Recipients::updateRecipient($request);
                $commodities = Commodity::updateCommodity($request);
                return $order->update($data);           
            } else {
                return null;
            }
        }
    }

    /**
     * Delete Order
     *
     * @param $id
     * @return array
     */
    public static function deleteOrder($id)
    {
        $order = Order::find($id);
        if ($order->status == 1) {
            return $order->delete();       
        } else return false;
    }

    /**
     * Delete Order
     *
     * @param $id
     * @return array
     */
    public static function deleteShipper($id)
    {
        $order = Order::find($id);
        if ($order->status == 2) {
            Acount::subMoney($order->shipper()->first()->account()->first()->id, 20000);
            $data['status'] = 1;
            $data['shipper_id'] = null;
            $order->update($data);
            return true;      
        } else return false;
    }

    /**
     * Rating Order
     *
     * @param $request
     * @return array
     */
    public static function rating($request)
    {
        $rating = Order::find($request->order_id);
        $data['rate'] = $request->rate;
        $updateRate = $rating->update($data);

        // update level of Shipper
        $updateLevel = Order::select('rate')->where('shipper_id', '=', $rating->shipper_id)->get();
        $count = 0;
        $sum = 0;
        foreach ($updateLevel as $key) {
            if ($key->rate != null) {
                $count++;
                $sum += $key->rate;
            }
        }
        if ($count > 0) {
            $level = $sum/$count;
            $dataShipper['level'] = $level;
            $shipper = Shipper::find($rating->shipper_id);
            $updatelevel = $shipper->update($dataShipper);
        } 
        if (!empty($updateRate) && !empty($updatelevel)) {
            return true;
        }
    }

    /**
     * Confirm Order
     *
     * @param $id
     * @return array
     */
    public static function confirmOrder($id)
    {
        $order = Order::find($id);
        if ($order->status == 3) {
            if ($order->type == 1) {
                Acount::subMoney($order->shipper()->first()->account()->first()->id, $order->price);
                Acount::subMoney($order->customer()->first()->account()->first()->id, $order->ship_cost);
            } else {
                Acount::subMoney($order->shipper()->first()->account()->first()->id, $order->price);
            }
            $data['status'] = 4;
            $order->update($data);
            return 1;       
        } elseif ($order->status == 5) {
            if ($order->type == 1) {
                Acount::addMoney($order->shipper()->first()->account()->first()->id, $order->price + $order->ship_cost);
            } else {
                Acount::addMoney($order->customer()->first()->account()->first()->id, $order->price);
            }
            $data['status'] =6;
            $order->update($data);
            return 2; 
        } else return 0;
    }

    /**
     * Calculator shipcost
     *
     * @param $id
     * @return array
     */
    public static function calculatorShipCost($from, $to)
    {
        $from = urlencode($from);
        $to = urlencode($to);

        $data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=en-EN&sensor=false");
        $data = json_decode($data);

        $time = 0;
        $distance = 0;

        foreach($data->rows[0]->elements as $road) {
            $time += $road->duration->value;
            $distance += $road->distance->value;
        }
        $km=$distance/1000;
        if ($km <= 10) {
            return 20000;
        } elseif ($km <= 20) {
            return 30000;
        } elseif ($km <= 30) {
            return 40000;
        } else return 50000;
    } 

    public static function haversine($lat1, $lng1, $lat2, $lng2)
    {

        // convert decimal degrees to radians
        $dLat = deg2rad($lat2 - $lat1);
        $dLng = deg2rad($lng2 - $lng1);
        $lat1 = deg2rad($lat1);
        $lat2 = deg2rad($lat2);

        // haversine formula
        $a = sin($dLat / 2) * sin($dLat / 2) +
             sin($dLng / 2) * sin($dLng / 2) *
             cos($lat1) * cos($lat2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        return 6371 * $c;
    }

    public static function getOrderNearest($lat, $lng, $radius)
    {
        $orders_nearest = array();
        $orders = Order::select('id', 'name', 'status', 'customer_id', 'shipper_id', 'type', 'ship_cost', 'price', 'ware_house_id')
            ->where('status', '=', 1)
            ->with('recipient')
            ->with('commodities')
            ->with('ware_house')
            ->with('customer.users')
            ->get();
        foreach ($orders as $order) {
            $lat1 = $order->ware_house->latitude;
            $lng1 = $order->ware_house->longtitude;
            $distance= Order::haversine($lat, $lng, $lat1, $lng1);
            if ($distance < $radius) {
                array_push($orders_nearest, $order);
            }
        }
        return $orders_nearest;
    }   

    public static function getOrderNearestToOrder($latRecipient, $lngRecipient, $latWareHouse, $lngWareHouse)
    {
        $orders_nearest = array();
        $orders = Order::select('id', 'name', 'status', 'customer_id', 'shipper_id', 'type', 'ship_cost', 'price', 'ware_house_id')
            ->where('status', '=', 1)
            ->with('recipient')
            ->with('commodities')
            ->with('ware_house')
            ->with('customer.users')
            ->get();
        foreach ($orders as $order) {
            $lat1 = $order->ware_house->latitude;
            $lng1 = $order->ware_house->longtitude;
            $lat2 = $order->recipient->latitude;
            $lng2 = $order->recipient->longtitude;
            if ($lat1 != $latWareHouse && $lat2 != $latRecipient) {
                $distance1 = Order::haversine($latWareHouse, $lngWareHouse, $lat1, $lng1);
                $distance2 = Order::haversine($latRecipient, $lngRecipient, $lat2, $lng2);
                if (($distance1 <= 2) && ($distance2 <= 2)) {
                    array_push($orders_nearest, $order);
                }
            }
        }
        return $orders_nearest;
    }    

    /**
     * receve Orders
     *
     * @param int $id, array $request
     * @return array
     */
    public static function receveOrders($id, $idShipper)
    {
        $order = Order::find($id);
        $countOrders = Order::where('shipper_id', '=', $idShipper)->where('status', '!=', 6)->count();
        if ($countOrders <= 5) {
            if ($order->status == 1) {
                $balances = Shipper::select('id', 'acount_id')->find($idShipper)->account()->first();
                if ($balances->balances < ($order->price + $order->ship_cost)) {
                    return 2;
                }
                $request['shipper_id'] = $idShipper;
                $request['status'] = 2;
                $update = $order->update($request);
                if (!empty($update)) {
                       return 1;
                   }   
            }
             return 3;    
        } else {
        return 4;
        }
    }

    /**
     * pickup Orders
     *
     * @param int $id, array $request
     * @return array
     */
    public static function pickupOrders($id, $idShipper)
    {
        $order = Order::find($id);
        if (($order->status == 2) && ($order->shipper_id == $idShipper) ) {
            $request['shipper_id'] = $idShipper;
            $request['status'] = 3;
            return $order->update($request);    
        } else return false;
    }
    
    /**
     * delivery Orders
     *
     * @param int $id, array $request
     * @return array
     */
    public static function deliveryOrders($id, $idShipper)
    {
        $order = Order::find($id);
        if ($order->status == 4) {
            $request['shipper_id'] = $idShipper;
            $request['status'] = 5;
            return $order->update($request);
        } else return false;
    }
}
