<?php

namespace App;

use App\User;
use App\Acount;
use App\WareHouse;
use App\Order;
use Hash;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'acount_id', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function users(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function account(){
        return $this->belongsTo('App\Acount', 'acount_id', 'id');
    }

    public function wareHouses(){
        return $this->hasMany('App\WareHouse');
    }

    public function orders(){
        return $this->hasMany('App\Order');
    }

    /**
     * Create and update new Customer
     *
     * @param  $event, int $shop_id
     * @return array
     */
    public static function createCustomer($request)
    {
    	$customer = new  Customer();
    		return $customer->create($request);
    }

    /**
     * List Customers
     *
     * @param  $event, int $shop_id
     * @return array
     */
    public static function listCustomer()
    {
        $customers = Customer::get();
        $i = 0;
        foreach ($customers as $customer) {
            $userOfCustomer = User::listUser($customer->user_id, null);
            $balance = Acount::find($customer->acount_id);
            $infoCustomer[$i]['id'] = $userOfCustomer->id;
            $infoCustomer[$i]['name'] = $userOfCustomer->name;
            $infoCustomer[$i]['phone'] = $userOfCustomer->phone;
            $infoCustomer[$i]['email'] = $userOfCustomer->email;
            $infoCustomer[$i]['address'] = $userOfCustomer->address;
            $infoCustomer[$i]['balances'] = $balance->balances;
            $i++;
        }
        if (!empty($infoCustomer)) {
            return $infoCustomer; 
        }
    }

    /**
     * Edit Customer
     *
     * @param int $id, array $request
     * @return array
     */
    public static function updateCustomer($id, $request)
    {
        $customer = User::find($id);
        if ($request == null) {
            return $customer;
        } else {
            if ($request['password'] == null) {
                $request['password'] = $customer->password;
            } else {
                $request['password'] = Hash::make($request['password']);
            }
            return $customer->update($request);
        }
    }

    /**
     * Delete Customer
     *
     * @param $id
     * @return array
     */
    public static function deleteCustomer($id)
    {
        $user = User::find($id);
        return $user->delete();       
    }

    /**
     * Delete Customer
     *
     * @param $id
     * @return array
     */
    public static function listWareHouse($id)
    {
        $customer = Customer::find($id);
        // return $customer->wareHouses();       
        // $warehouses=$customer->wareHouses();       
        // foreach ($warehouses as $warehouse) {
        //     echo $warehouse;
        // }

        echo $customer;
    }
}
