<?php

namespace App;

use App\Order;
use Illuminate\Database\Eloquent\Model;

class Commodity extends Model
{
    protected $table = 'commodities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'count', 'price', 'weigh', 'order_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];


    public function orders() {
        return $this->belongsTo('App\Order');
    }

    /**
     * Create new Recipients
     *
     * @param  $request
     * @return array
     */
    public static function createCommodity($request)
    {
        $commodity = new Commodity();
        $request['name'] = $request['name-commodity'];
        $request['count'] = $request['count-commodity'];
        $request['price'] = $request['price-commodity'];
        $request['weigh'] = $request['weight-commodity'];
        return $commodity->create($request);
    }

    /**
     * Update new Recipients
     *
     * @param  $request
     * @return array
     */
    public static function updateCommodity($request)
    {
        $commodity = Commodity::where('order_id', '=', $request['order_id']);
        $data['name'] = $request['name-commodity'];
        $data['count'] = $request['count-commodity'];
        $data['price'] = $request['price-commodity'];
        $data['weigh'] = $request['weight-commodity'];
        return $commodity->update($data);
    }
}
