<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('aaa/{username}', 'ChatController@testabc');
Route::post('sendMessage', 'ChatController@store');
Route::get('ajax', 'ChatController@ajax');
Route::get('index-customer', function() {
    return  view('customer.index');
});

Route::post('/ship/login/', 'Auth\LoginController@loginApp');

Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

//register customer
Route::get('register', 'MemberController@register');
Route::post('register', 'MemberController@postRegister');

// admin
Route::group(['prefix'=>'admin', 'middleware' => ['admin']], function(){
    Route::group(['prefix'=>'admin'], function(){
        Route::get('create',['as'=>'admin.admin.getCreate', 'uses'=>'UserController@getCreate']);
        Route::post('create',['as'=>'admin.admin.postCreate', 'uses'=>'UserController@postCreate']);
        Route::get('list',['as'=>'admin.admin.list', 'uses'=>'UserController@list']);
        Route::get('edit/{id}',['as'=>'admin.admin.getEdit', 'uses'=>'UserController@getEdit']);
        Route::post('edit/{id}',['as'=>'admin.admin.postEdit', 'uses'=>'UserController@postEdit']);
        Route::get('delete/{id}', ['as' => 'admin.admin.delete', 'uses' => 'UserController@delete']);
    });
        Route::group(['prefix'=>'events'], function(){
        Route::get('create',['as'=>'admin.events.getCreate', 'uses'=>'EventController@getCreate']);
        Route::post('create',['as'=>'admin.events.postCreate', 'uses'=>'EventController@postCreate']);
        Route::get('list',['as'=>'admin.events.list', 'uses'=>'EventController@list']);
        Route::get('edit/{id}',['as'=>'admin.events.getEdit', 'uses'=>'EventController@getEdit']);
        Route::post('edit/{id}',['as'=>'admin.events.postEdit', 'uses'=>'EventController@postEdit']);
        Route::get('delete/{id}', ['as' => 'admin.events.delete', 'uses' => 'EventController@delete']);
    });
    Route::group(['prefix'=>'customers'], function(){
        Route::get('create',['as'=>'admin.customers.getCreate', 'uses'=>'CustomerController@getCreate']);
        Route::post('create',['as'=>'admin.customers.postCreate', 'uses'=>'CustomerController@postCreate']);
        Route::get('list',['as'=>'admin.customers.list', 'uses'=>'CustomerController@list']);
        Route::get('edit/{id}',['as'=>'admin.customers.getEdit', 'uses'=>'CustomerController@getEdit']);
        Route::post('edit/{id}',['as'=>'admin.customers.postEdit', 'uses'=>'CustomerController@postEdit']);
        Route::get('delete/{id}', ['as' => 'admin.customers.delete', 'uses' => 'CustomerController@delete']);
    });
    Route::group(['prefix'=>'shippers'], function(){
        Route::get('create',['as'=>'admin.shippers.getCreate', 'uses'=>'ShipperController@getCreate']);
        Route::post('create',['as'=>'admin.shippers.postCreate', 'uses'=>'ShipperController@postCreate']);
        Route::get('list',['as'=>'admin.shippers.list', 'uses'=>'ShipperController@list']);
        Route::get('edit/{id}',['as'=>'admin.shippers.getEdit', 'uses'=>'ShipperController@getEdit']);
        Route::post('edit/{id}',['as'=>'admin.shippers.postEdit', 'uses'=>'ShipperController@postEdit']);
        Route::get('delete/{id}', ['as' => 'admin.shippers.delete', 'uses' => 'ShipperController@delete']);
    });
    Route::group(['prefix'=>'orders'], function(){
        Route::get('list',['as'=>'admin.orders.list', 'uses'=>'OrderController@listAllOrder']);
        Route::get('removeShipper/{id}',['as'=>'admin.orders.removeShipper', 'uses'=>'OrderController@deleteShipper']);
        Route::get('delete/{id}', ['as' => 'admin.orders.delete', 'uses' => 'OrderController@delete']);
    });
    Route::group(['prefix'=>'account'], function(){
        Route::get('edit/{id}',['as'=>'admin.account.getEdit', 'uses'=>'UserController@getEdit']);
        Route::post('edit/{id}',['as'=>'admin.account.postEdit', 'uses'=>'UserController@postEdit']);
    });
});

// customer
Route::group(['prefix'=>'customer', 'middleware' => ['customer']], function(){
    Route::group(['prefix'=>'orders'], function(){
        Route::get('create',['as'=>'customer.orders.create', 'uses'=>'OrderController@getCreate']);
        Route::post('create',['as'=>'customer.orders.postCreate', 'uses'=>'OrderController@postCreate']);
        Route::get('list',['as'=>'customer.orders.list', 'uses'=>'OrderController@list']);
        Route::get('edit/{id}',['as'=>'customer.orders.getEdit', 'uses'=>'OrderController@getEdit']);
        Route::post('edit/{id}',['as'=>'customer.orders.postEdit', 'uses'=>'OrderController@postEdit']);
        Route::get('delete/{id}', ['as' => 'customer.orders.delete', 'uses' => 'OrderController@delete']);
        Route::get('confirm/{id}', ['as' => 'customer.orders.confirm', 'uses' => 'OrderController@confirm']);
        Route::get('confirm-orders', ['as' => 'customer.orders.confirmOrders', 'uses' => 'OrderController@confirmOrders']);
        Route::get('history-orders', ['as' => 'customer.orders.historyOrders', 'uses' => 'OrderController@historyOrders']);
        Route::post('rating', ['as' => 'customer.orders.rating', 'uses' => 'OrderController@rating']);
    });
    Route::get('list-shippers', ['as' => 'customer.shippers.listShippers', 'uses' => 'CustomerController@listShippers']);
    Route::group(['prefix'=>'account'], function(){
        Route::get('edit/{id}',['as'=>'customer.account.getEdit', 'uses'=>'CustomerController@getEditAccount']);
        Route::post('edit/{id}',['as'=>'customer.account.postEdit', 'uses'=>'CustomerController@postEditAccount']);
    });
});




Route::get('check-account', ['as' => 'checkAccount', 'uses' => 'AccountController@checkAcount']);

Route::get('/bridgeabc', function() {
    $pusher = App::make('pusher');
// $pusher = new Pusher("72f8c2eed83c7dd0f357", "2e876e5ffc8fa8802205", "276664");
$pusher->notify(
  array("kittens"),
  array(
    'fcm' => array(
      'data' => array(
        'title' => 'sdsd',
        'icon' => 'androidlogo'
      ),
    ),
    'webhook_url' => 'https://example.com/endpoint',
    'webhook_level' => 'INFO',
 )
);
// return view('home');
});

Route::post('pusher/auth', function() {
    $user = auth()->user();
    $id = explode('-', $_POST['channel_name']);
    if ($user->id == $id[2]) {
        $pusher = new Pusher('72f8c2eed83c7dd0f357', '2e876e5ffc8fa8802205', 276664);
        echo $pusher->socket_auth($_POST['channel_name'], $_POST['socket_id']);
        return ;
    }else {
        header('', true, 403);
        echo "Forbidden";
        return;
    }
});

Route::get('/bridge', function() {
    $pusher = App::make('pusher');

    $pusher->trigger( 'test-channel',
                      'test-event', 
                      array('text' => 'Preparing the Pusher Laracon.eu workshop!'));

    // return view('pusher');
});

Route::get('/account', function() {
    $account = new App\Acount();
    $data['id'] = 20130021;
    $data['balances'] = 20000000;
    $account->create($data);
});