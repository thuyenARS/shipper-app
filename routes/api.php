<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
Route::post('ship/login', 'Api\LoginController@authenticate'); 
Route::post('ship/register', 'MemberController@postRegisterShipper'); 
Route::get('auth/me', 'Api\LoginController@getAuthenticatedUser')->middleware('jwt.auth'); 
Route::get('ship/logout', 'Api\LoginController@logout')->middleware('jwt.auth'); 
Route::group(['prefix'=>'ship', 'middleware'=> 'jwt.auth'], function(){
    Route::get('orders',['as'=>'ship.orders', 'uses'=>'OrderController@getShipOrder']);
    Route::get('info',['as'=>'ship.info', 'uses'=>'ShipperController@info']);
    Route::get('order-detail/{id}',['as'=>'ship.ordersDetail', 'uses'=>'OrderController@ordersDetail']);
    Route::get('received',['as'=>'ship.received', 'uses'=>'OrderController@getReceivedOrders']);
	Route::get('orders-nearest/{lat}/{lng}/{radius}',['as'=>'ship.ordersnearest', 'uses'=>'OrderController@getOrderNearest']);
	Route::get('orders-nearest-to-order/{latRecipient}/{lngRecipient}/{latWareHouse}/{lngWareHouse}',['as'=>'ship.ordersnearest', 'uses'=>'OrderController@getOrderNearestToOrder']);
	Route::get('receive-order/{id}', ['as'=>'ship.receivedorders', 'uses'=>'OrderController@receveOrders']);
	Route::get('cancel-order/{id}', ['as'=>'ship.receivedorders', 'uses'=>'OrderController@cancelOrder']);
	Route::get('pickup-order/{id}', ['as'=>'ship.pickuporders', 'uses'=>'OrderController@pickupOrders']);
	Route::get('delivery-order/{id}', ['as'=>'ship.deliveryorders', 'uses'=>'OrderController@deliveryOrders']);
	Route::get('events', ['as'=>'ship.events', 'uses'=>'EventController@listEventForShipper']);
});